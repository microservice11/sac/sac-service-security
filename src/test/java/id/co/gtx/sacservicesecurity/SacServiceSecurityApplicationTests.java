package id.co.gtx.sacservicesecurity;

import id.co.gtx.sacservicesecurity.model.entity.postgresql.User;
import id.co.gtx.sacservicesecurity.repository.postgresql.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDate;
import java.time.LocalDateTime;

@ActiveProfiles("dev")
@SpringBootTest
class SacServiceSecurityApplicationTests {

	private final UserRepository userRepository;

	private final PasswordEncoder passwordEncoder;

	@Autowired
	SacServiceSecurityApplicationTests(UserRepository userRepository, PasswordEncoder passwordEncoder) {
		this.userRepository = userRepository;
		this.passwordEncoder = passwordEncoder;
	}

	@Test
	void contextLoads() {
		//create user super admin
		User user = User.builder()
				.username("administrator")
				.firstName("Super Administrator")
				.password(passwordEncoder.encode("nimda@1928"))
				.email("admin@gmail.com")
				.enabled(true)
				.expiredDate(LocalDate.parse("9999-12-31"))
				.expiredPasswordDate(LocalDate.parse("9999-12-31"))
				.telegramId(300866978L)
				.defaultUser(true)
				.failPass((short) 0)
				.limitLogin((short) 1)
				.limitFail((short) 99)
				.createAt(LocalDateTime.now())
				.build();

		User userSaved = userRepository.save(user);
		Assertions.assertNotNull(userSaved);
	}
}
