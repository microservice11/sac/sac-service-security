package id.co.gtx.sacservicesecurity;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"id.co.gtx"})
@OpenAPIDefinition(info =
@Info(title = "Security Service API",
		version = "${spring.application.version}",
		description = "Documentation Security Service API"))
public class SacServiceSecurityApplication {

	public static void main(String[] args) {
		SpringApplication.run(SacServiceSecurityApplication.class, args);
	}

}
