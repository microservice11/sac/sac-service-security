package id.co.gtx.sacservicesecurity.exception;

import id.co.gtx.sacmodules.dto.DtoError;
import id.co.gtx.sacmodules.dto.DtoResponse;
import id.co.gtx.sacmodules.dto.ErrorCode;
import id.co.gtx.sacmodules.dto.StatusCode;
import id.co.gtx.sacmodules.enumz.Status;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler({IllegalArgumentException.class})
    public ResponseEntity<DtoResponse<?>> handleIllegalArgumentException(IllegalArgumentException ex, WebRequest request) {
        ex.printStackTrace();
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "Error Argument Service Backend", null,
                new DtoError(ErrorCode.ILLEGAL_ARGUMENT_EXCEPTION, ex.getMessage())));
    }

    @ExceptionHandler({NullPointerException.class})
    public ResponseEntity<DtoResponse<?>> handleNullPointerException(NullPointerException ex, WebRequest request) {
        ex.printStackTrace();
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "Error Argument Service Backend", null,
                new DtoError(ErrorCode.NULL_POINTER_EXCEPTION, ex.getMessage())));
    }

    @ExceptionHandler({OAuth2AuthenticationException.class})
    public ResponseEntity<DtoResponse<?>> handleOperationException(OAuth2AuthenticationException ex, WebRequest request) {
        ex.printStackTrace();
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "Gagal Login", null,
                new DtoError(ex.getError().getErrorCode(), ex.getError().getDescription())));
    }
}
