package id.co.gtx.sacservicesecurity.repository.postgresql;

import id.co.gtx.sacservicesecurity.model.entity.postgresql.Menu;
import id.co.gtx.sacservicesecurity.model.pk.MenuPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface MenuRepository extends JpaRepository<Menu, MenuPK> {
    List<Menu> findByParentId(String parentId);

    List<Menu> findByParentIdIsNullAndFormAppIsNull();

    @Query("select r.menus from Role r where r.name in :roleName ")
    Set<Menu> findByRoleName(List<String> roleName);

    @Query(value = "select cast(coalesce(max(id), '0') as integer ) + 1 as id from menu", nativeQuery = true)
    Integer findByIdMax();
}
