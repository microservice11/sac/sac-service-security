package id.co.gtx.sacservicesecurity.repository.postgresql;

import id.co.gtx.sacservicesecurity.model.entity.postgresql.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleRepository extends JpaRepository<Role, String> {

    @Query("select r from Role r where r.name in ?1 order by r.id")
    List<Role> findByNameIn(List<String> names);
}
