package id.co.gtx.sacservicesecurity.repository.redis;

import id.co.gtx.sacservicesecurity.model.entity.redis.Otp;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OtpRepository extends CrudRepository<Otp, String> {
    List<Otp> findFirstByUsername(String username);

    List<Otp> findFirstByUsernameAndOtp(String username, String otp);
}
