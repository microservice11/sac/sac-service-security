package id.co.gtx.sacservicesecurity.repository.postgresql;

import id.co.gtx.sacservicesecurity.model.entity.postgresql.Group;
import id.co.gtx.sacservicesecurity.model.pk.GroupPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GroupRepository extends JpaRepository<Group, GroupPK> {
    List<Group> findByUsername(String username);

    void deleteByUsername(String username);
}
