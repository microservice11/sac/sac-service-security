package id.co.gtx.sacservicesecurity.repository.postgresql;

import id.co.gtx.sacservicesecurity.model.entity.postgresql.ResourceAccess;
import id.co.gtx.sacservicesecurity.model.pk.ResourceAccessPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ResourceAccessRepository extends JpaRepository<ResourceAccess, ResourceAccessPK> {

    void deleteByUsername(String username);
}
