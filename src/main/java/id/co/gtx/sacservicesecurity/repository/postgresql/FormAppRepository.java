package id.co.gtx.sacservicesecurity.repository.postgresql;

import id.co.gtx.sacservicesecurity.model.entity.postgresql.FormApp;
import id.co.gtx.sacservicesecurity.model.pk.FormAppPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface FormAppRepository extends JpaRepository<FormApp, FormAppPK> {

    @Query(value = "select cast(coalesce(max(id), '0') as integer ) + 1 as id from form_app", nativeQuery = true)
    Integer findByIdMax();
}
