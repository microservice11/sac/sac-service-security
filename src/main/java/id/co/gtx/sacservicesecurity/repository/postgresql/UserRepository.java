package id.co.gtx.sacservicesecurity.repository.postgresql;

import id.co.gtx.sacservicesecurity.model.dto.DtoUserGetName;
import id.co.gtx.sacservicesecurity.model.entity.postgresql.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, String> {
    Optional<User> findByEmail(String email);

    Optional<User> findByEmailAndUsername(String email, String username);

    Optional<User> findByEmailAndUsernameNot(String email, String username);

    @Query("select new id.co.gtx.sacservicesecurity.model.dto.DtoUserGetName(u.firstName, u.lastName) from User u where u.username = ?1")
    Optional<DtoUserGetName> findNameByUsername(String username);

    @Modifying(flushAutomatically = true)
    @Query("update User u set u.password = :#{#user.password} where u.username = :#{#user.username} ")
    void updateUserPassword(User user);

    @Modifying(flushAutomatically = true)
    @Query("update User u set u.failPass = :#{#user.failPass} where u.username = :#{#user.username} ")
    void updateUserFailPass(User user);
}
