package id.co.gtx.sacservicesecurity.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import javax.persistence.Table;
import java.time.format.DateTimeFormatter;
import java.util.Map;

public abstract class AuditEntity<T> {

    private ObjectMapper mapper;

    public AuditEntity() {
        JavaTimeModule javaTimeModule = new JavaTimeModule();
        javaTimeModule.addSerializer(new LocalDateTimeSerializer(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS")));
        javaTimeModule.addSerializer(new LocalDateSerializer(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        mapper = new ObjectMapper();
        mapper.registerModule(javaTimeModule);
    }

    @JsonIgnore
    protected abstract T getPk();

    @JsonIgnore
    public String getTableName() {
        return this.getClass().getAnnotation(Table.class).name();
    }

    @JsonIgnore
    public String getStringPk() {
        return getMapPk().toString().replace("{",  "").replace("}", "");
    }

    @JsonIgnore
    public Map<String, Object> getMapPk() {
        if (getPk() instanceof Map) {
            return (Map<String, Object>) getPk();
        } else {
            return mapper.convertValue(getPk(), new TypeReference<Map<String, Object>>() {});
        }
    }

    @JsonIgnore
    public Map<String, Object> getMapData() {
        return mapper.convertValue(this, new TypeReference<Map<String, Object>>() {});
    }
}
