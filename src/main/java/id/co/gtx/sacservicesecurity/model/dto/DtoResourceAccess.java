package id.co.gtx.sacservicesecurity.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DtoResourceAccess implements Serializable {
    private static final long serialVersionUID = 3776108873984562536L;

    @JsonProperty("client_id")
    @NotBlank(message = "Client ID harus diisi")
    private String clientId;

    @NotNull(message = "Role ID harus diisi")
    private Set<DtoRoleGet> roles;
}
