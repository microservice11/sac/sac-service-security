package id.co.gtx.sacservicesecurity.model.dto;

import id.co.gtx.sacmodules.dto.DtoGroup;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.SpringSecurityCoreVersion;
import org.springframework.security.core.userdetails.User;
import org.springframework.util.Assert;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.LocalDateTime;
import java.util.*;

public class DtoUserDetails extends User {
    private static final long serialVersionUID = 7202164631027098194L;

    private final String username;
    private String password;
    private final boolean accountNonExpired;
    private final boolean accountNonLocked;
    private final boolean credentialsNonExpired;
    private final boolean enabled;
    private final Set<GrantedAuthority> authorities;
    private final Set<DtoGroup> groups;
    private boolean defaultUser;
    private LocalDateTime lastLogin;

    public DtoUserDetails(String username, String password, Collection<? extends GrantedAuthority> authorities, LocalDateTime lastLogin) {
        this(username, password, true, true, true, true, authorities, lastLogin);
    }

    public DtoUserDetails(String username,
                          String password,
                          boolean enabled,
                          boolean accountNonExpired,
                          boolean credentialsNonExpired,
                          boolean accountNonLocked,
                          Collection<? extends GrantedAuthority> authorities,
                          LocalDateTime lastLogin) {
        this(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities, new HashSet<>(), false, lastLogin);
    }

    public DtoUserDetails(String username,
                          String password,
                          boolean enabled,
                          boolean accountNonExpired,
                          boolean credentialsNonExpired,
                          boolean accountNonLocked,
                          Collection<? extends GrantedAuthority> authorities,
                          Collection<? extends DtoGroup> groups,
                          boolean defaultUser,
                          LocalDateTime lastLogin) {
        super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
        this.username = username;
        this.password = password;
        this.accountNonExpired = accountNonExpired;
        this.accountNonLocked = accountNonLocked;
        this.credentialsNonExpired = credentialsNonExpired;
        this.enabled = enabled;
        this.authorities = Collections.unmodifiableSet(sortAuthorities(authorities));
        this.groups = Collections.unmodifiableSet(sortGroups(groups));
        this.defaultUser = defaultUser;
        this.lastLogin = lastLogin;
    }


    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return accountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return credentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public void eraseCredentials() {
        this.password = null;
    }

    public boolean isDefaultUser() {
        return defaultUser;
    }

    public LocalDateTime getLastLogin() {
        return lastLogin;
    }

    public Set<DtoGroup> getGroups() {
        return groups;
    }

    private static SortedSet<GrantedAuthority> sortAuthorities(Collection<? extends GrantedAuthority> authorities) {
        Assert.notNull(authorities, "Cannot pass a null GrantedAuthority collection");
        // Ensure array iteration order is predictable (as per
        // UserDetails.getAuthorities() contract and SEC-717)
        SortedSet<GrantedAuthority> sortedAuthorities = new TreeSet<>(new AuthorityComparator());
        for (GrantedAuthority grantedAuthority : authorities) {
            Assert.notNull(grantedAuthority, "GrantedAuthority list cannot contain any null elements");
            sortedAuthorities.add(grantedAuthority);
        }
        return sortedAuthorities;
    }

    private static Set<DtoGroup> sortGroups(Collection<? extends DtoGroup> groups) {
        Assert.notNull(groups, "Cannot pass a null DtoGroup collection");
        // Ensure array iteration order is predictable (as per
        // UserDetails.getAuthorities() contract and SEC-717)
        Set<DtoGroup> groupSet = new HashSet<>(groups);
        for (DtoGroup group : groups) {
            Assert.notNull(group, "DtoGroup list cannot contain any null elements");
            groupSet.add(group);
        }
        return groupSet;
    }

    private static class AuthorityComparator implements Comparator<GrantedAuthority>, Serializable {

        private static final long serialVersionUID = SpringSecurityCoreVersion.SERIAL_VERSION_UID;

        @Override
        public int compare(GrantedAuthority g1, GrantedAuthority g2) {
            // Neither should ever be null as each entry is checked before adding it to
            // the set. If the authority is null, it is a custom authority and should
            // precede others.
            if (g2.getAuthority() == null) {
                return -1;
            }
            if (g1.getAuthority() == null) {
                return 1;
            }
            return g1.getAuthority().compareTo(g2.getAuthority());
        }

    }
}
