package id.co.gtx.sacservicesecurity.model.entity.redis;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import id.co.gtx.sacmodules.enumz.OtpType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

import javax.persistence.Id;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@RedisHash(value = "otp", timeToLive = 60)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Otp {

    @Id
    @JsonIgnore
    private String id = UUID.randomUUID().toString();

    @Indexed
    private String username;
    private String email;

    @JsonProperty("telegram_id")
    private Long telegramId;

    private String password;

    @Indexed
    private String otp;

    @JsonProperty("ip_client")
    private String ipClient;

    @JsonProperty("otp_type")
    private OtpType otpType;

    @JsonProperty("create_at")
    private LocalDateTime createAt;

}
