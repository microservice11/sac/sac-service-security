package id.co.gtx.sacservicesecurity.model.pk;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MenuPK implements Serializable {
    private static final long serialVersionUID = -4848225800223923307L;

    private String id;

    @JsonProperty("menu_type")
    private String menuType;
}
