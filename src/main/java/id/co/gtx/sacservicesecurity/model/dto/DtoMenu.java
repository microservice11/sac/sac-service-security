package id.co.gtx.sacservicesecurity.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DtoMenu implements Serializable {
    private static final long serialVersionUID = -7442670873236073867L;

    @NotBlank(message = "Menu ID harus diisi")
    private String id;

    @JsonProperty("menu_type")
    @NotBlank(message = "Menu Type harus diisi")
    private String menuType;

    @NotBlank(message = "Menu Nama harus diisi")
    private String name;

    @JsonProperty("parent_id")
    private String parentId;

    private String path;

    @JsonProperty("path_parameter")
    private String pathParameter;

    @NotNull(message = "Menu Sequence harus diisi")
    private Integer seq;

    @JsonProperty("form_app")
    private DtoFormApp formApp;

    @JsonProperty(value = "create_at", access = JsonProperty.Access.READ_ONLY)
    private LocalDateTime createAt;

    @JsonProperty(value = "update_at", access = JsonProperty.Access.READ_ONLY)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private LocalDateTime updateAt;
}
