package id.co.gtx.sacservicesecurity.model.entity.postgresql;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import id.co.gtx.sacservicesecurity.model.dto.AuditEntity;
import id.co.gtx.sacservicesecurity.service.impl.AuditingEntitySecurityListener;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

@Getter
@Setter
@Entity
@Builder
@ToString
@AllArgsConstructor
@Table(name = "role")
@RequiredArgsConstructor
@EntityListeners(AuditingEntitySecurityListener.class)
public class Role extends AuditEntity<Map<String, String>> implements Serializable {
    private static final long serialVersionUID = 5992251733027509475L;

    @Id
    @Column(length = 2)
    private String id;

    @Column(length = 20)
    private String name;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    @JoinTable(name = "role_menu",
            joinColumns = {@JoinColumn(name = "role_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "menu_id", referencedColumnName = "id"),
                    @JoinColumn(name = "menu_type", referencedColumnName = "menu_type")}
    )
    private Set<Menu> menus;

    @JsonBackReference
    @ManyToMany(mappedBy = "roles")
    @JsonProperty("resource_accesses")
    @ToString.Exclude
    private Set<ResourceAccess> resourceAccesses;

    @JsonProperty("create_at")
    @Column(name = "create_at", nullable = false)
    private LocalDateTime createAt;

    @JsonProperty("update_at")
    @Column(name = "update_at")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private LocalDateTime updateAt;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Role role = (Role) o;
        return id != null && Objects.equals(id, role.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    @Override
    public Map<String, String> getPk() {
        Map<String, String> map = new HashMap<>();
        map.put("id", id);
        return map;
    }
}
