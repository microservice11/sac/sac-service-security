package id.co.gtx.sacservicesecurity.model.pk;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GroupPK implements Serializable {
    private static final long serialVersionUID = 2465745273945165711L;

    private String username;

    @JsonProperty("group_id")
    private String groupId;
}
