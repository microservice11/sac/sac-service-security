package id.co.gtx.sacservicesecurity.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DtoMenuGet implements Serializable {
    private static final long serialVersionUID = -7442670873236073867L;

    private String id;

    @JsonProperty("menu_type")
    private String menuType;

    private String name;

    @JsonProperty("parent_id")
    private String parentId;

    private String path;

    @JsonProperty("path_parameter")
    private String pathParameter;

    private Integer seq;

    @JsonProperty("form_app")
    private DtoMenuFormApp formApp;
}
