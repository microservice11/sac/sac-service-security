package id.co.gtx.sacservicesecurity.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DtoFormApp implements Serializable {
    private static final long serialVersionUID = -8577428791708505379L;

    @NotBlank(message = "Form ID harus diisi")
    private String id;

    @JsonProperty("form_type")
    @NotBlank(message = "Form Type harus diisi")
    private String formType;

    @NotBlank(message = "Form Nama harus diisi")
    private String name;

    @NotBlank(message = "Form Path harus diisi")
    private String form;

    @JsonProperty(value = "create_at", access = JsonProperty.Access.READ_ONLY)
    private LocalDateTime createAt;

    @JsonProperty(value = "update_at", access = JsonProperty.Access.READ_ONLY)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private LocalDateTime updateAt;
}
