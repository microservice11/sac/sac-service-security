package id.co.gtx.sacservicesecurity.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DtoUserGet implements Serializable {
    private static final long serialVersionUID = -7129813674951041345L;

    @NotBlank(message = "Username harus diisi")
    private String username;

    @JsonProperty("first_name")
    @NotBlank(message = "First Name harus diisi")
    private String firstName;

    @JsonProperty("last_name")
    private String lastName;

    @NotBlank(message = "Email harus diisi")
    @Email(message = "Format Email Tidak Sesuai")
    private String email;

    @NotNull(message = "Enable harus diisi")
    private boolean enabled;

    @JsonProperty("expired_date")
    @NotNull(message = "Expired Date harus diisi")
    private LocalDate expiredDate;

    @JsonProperty("expired_password_date")
    @NotNull(message = "Expired Password Date harus diisi")
    private LocalDate expiredPasswordDate;

    @JsonProperty("resource_accesses")
    private Set<DtoResourceAccess> resourceAccesses;

    @NotNull(message = "Telegram ID harus diisi")
    @JsonProperty("telegram_id")
    private Long telegramId;

    @NotNull(message = "Default User harus diisi")
    @JsonProperty("default_user")
    private boolean defaultUser;

    @NotNull(message = "Fail Pass harus diisi")
    @JsonProperty("fail_pass")
    private Short failPass;

    @NotNull(message = "Limit Login harus diisi")
    @JsonProperty("limit_login")
    private Short limitLogin;

    @NotNull(message = "Limit Fail harus diisi")
    @JsonProperty("limit_fail")
    private Short limitFail;

    private Set<DtoGroup> groups;

    @JsonProperty("create_at")
    private LocalDateTime createAt;

    @JsonProperty("update_at")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private LocalDateTime updateAt;
}
