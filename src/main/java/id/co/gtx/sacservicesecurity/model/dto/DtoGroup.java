package id.co.gtx.sacservicesecurity.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DtoGroup implements Serializable {
    private static final long serialVersionUID = -1577039242479546899L;

    private String username;

    @JsonProperty("group_id")
    private String groupId;

    @JsonProperty("group_parent")
    private String groupParent;

    private Integer type;
}
