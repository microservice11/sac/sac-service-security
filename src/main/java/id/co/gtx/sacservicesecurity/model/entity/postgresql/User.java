package id.co.gtx.sacservicesecurity.model.entity.postgresql;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import id.co.gtx.sacservicesecurity.model.dto.AuditEntity;
import id.co.gtx.sacservicesecurity.service.impl.AuditingEntitySecurityListener;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

@Getter
@Setter
@Entity
@Builder
@ToString
@AllArgsConstructor
@Table(name = "users")
@RequiredArgsConstructor
@EntityListeners(AuditingEntitySecurityListener.class)
public class User extends AuditEntity<Map<String, String>> implements Serializable {
    private static final long serialVersionUID = -8896560100388963192L;

    @Id
    @Column(name = "username", length = 20, nullable = false)
    private String username;

    @Column(name = "first_name", length = 50, nullable = false)
    @JsonProperty("first_name")
    private String firstName;

    @Column(name = "last_name", length = 50)
    @JsonProperty("last_name")
    private String lastName;

    @Column(name = "password", length = 100, nullable = false)
    private String password;

    @Column(name = "email",  length = 100, nullable = false)
    private String email;

    @Column(name = "enabled", nullable = false)
    private boolean enabled;

    @JsonProperty("expired_date")
    @Column(name = "expired_date", nullable = false)
    private LocalDate expiredDate;

    @JsonProperty("expired_password_date")
    @Column(name = "expired_password_date", nullable = false)
    private LocalDate expiredPasswordDate;

    @ToString.Exclude
    @JsonProperty("resource_accesses")
    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.REMOVE})
    private Set<ResourceAccess> resourceAccesses;

    @JsonProperty("telegram_id")
    @Column(name = "telegram_id", nullable = false)
    private Long telegramId;

    @JsonProperty("default_user")
    @Column(name = "default_user", nullable = false)
    private boolean defaultUser;

    @JsonProperty("fail_pass")
    @Column(name = "fail_pass", nullable = false)
    private Short failPass;

    @JsonProperty("limit_login")
    @Column(name = "limit_login", nullable = false)
    private Short limitLogin;

    @JsonProperty("limit_fail")
    @Column(name = "limit_fail", nullable = false)
    private Short limitFail;

    @ToString.Exclude
    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.REMOVE})
    private Set<Group> groups;

    @JsonProperty("create_at")
    @Column(name = "create_at", nullable = false)
    private LocalDateTime createAt;

    @JsonProperty("update_at")
    @Column(name = "update_at")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private LocalDateTime updateAt;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        User user = (User) o;
        return username != null && Objects.equals(username, user.username);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    @Override
    protected Map<String, String> getPk() {
        Map<String, String> map = new HashMap<>();
        map.put("username", username);
        return map;
    }
}
