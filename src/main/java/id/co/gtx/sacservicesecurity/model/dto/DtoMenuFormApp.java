package id.co.gtx.sacservicesecurity.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DtoMenuFormApp implements Serializable {
    private static final long serialVersionUID = -6142348456156786365L;

    private String name;

    private String form;
}
