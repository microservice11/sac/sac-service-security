package id.co.gtx.sacservicesecurity.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DtoRoleGet implements Serializable {
    private static final long serialVersionUID = -5998081062889232832L;

    @NotBlank(message = "ID harus diisi")
    private String id;

    @NotBlank(message = "Nama Role harus diisi")
    private String name;
}
