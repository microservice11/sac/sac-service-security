package id.co.gtx.sacservicesecurity.model.pk;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResourceAccessPK implements Serializable {
    private static final long serialVersionUID = 5623090802158488651L;

    private String username;

    @JsonProperty("client_id")
    private String clientId;
}
