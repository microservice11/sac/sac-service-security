package id.co.gtx.sacservicesecurity.model.entity.postgresql;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import id.co.gtx.sacservicesecurity.model.dto.AuditEntity;
import id.co.gtx.sacservicesecurity.model.pk.FormAppPK;
import id.co.gtx.sacservicesecurity.service.impl.AuditingEntitySecurityListener;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@Entity
@Builder
@ToString
@AllArgsConstructor
@RequiredArgsConstructor
@IdClass(FormAppPK.class)
@Table(name = "form_app")
@EntityListeners(AuditingEntitySecurityListener.class)
public class FormApp extends AuditEntity<FormAppPK> implements Serializable {
    private static final long serialVersionUID = -5565408939395352645L;

    @Id
    @Column(length = 10)
    private String id;

    @Id
    @JsonProperty("form_type")
    @Column(name = "form_type", length = 3, nullable = false)
    private String formType;

    @Column(length = 125, nullable = false)
    private String name;

    @Column(length = 200, nullable = false)
    private String form;

    @JsonBackReference
    @OneToMany(mappedBy = "formApp", fetch = FetchType.EAGER)
    private List<Menu> menus;

    @JsonProperty("create_at")
    @Column(name = "create_at", nullable = false)
    private LocalDateTime createAt;

    @JsonProperty("update_at")
    @Column(name = "update_at")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private LocalDateTime updateAt;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        FormApp formApp = (FormApp) o;
        return id != null && Objects.equals(id, formApp.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    @Override
    protected FormAppPK getPk() {
        return new FormAppPK(id, formType);
    }
}
