package id.co.gtx.sacservicesecurity.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

import java.io.Serializable;

public class DtoUserGetName implements Serializable {
    private static final long serialVersionUID = 1028499250946872602L;

    @Getter
    @JsonProperty("full_name")
    private String fullName;

    public DtoUserGetName(String firstName, String lastName) {
        fullName = firstName;
        if (!"".equals(lastName) && lastName != null)
            fullName = fullName + " " + lastName;
    }
}
