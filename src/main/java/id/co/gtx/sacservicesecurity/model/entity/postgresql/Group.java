package id.co.gtx.sacservicesecurity.model.entity.postgresql;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import id.co.gtx.sacservicesecurity.model.dto.AuditEntity;
import id.co.gtx.sacservicesecurity.model.pk.GroupPK;
import id.co.gtx.sacservicesecurity.service.impl.AuditingEntitySecurityListener;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Getter
@Setter
@Entity
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
@IdClass(GroupPK.class)
@Table(name = "groups")
@EntityListeners(AuditingEntitySecurityListener.class)
public class Group extends AuditEntity<GroupPK> implements Serializable {
    private static final long serialVersionUID = 664700003813751137L;

    @Id
    @Column(name = "username", length = 20, nullable = false)
    private String username;

    @Id
    @JsonProperty("group_id")
    @Column(name = "group_id", length = 10, nullable = false)
    private String groupId;

    @JsonProperty("group_parent")
    @Column(name = "group_parent", length = 10)
    private String groupParent;

    @Column(nullable = false)
    private Short type;

    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "username", referencedColumnName = "username", insertable = false, updatable = false)
    })
    @JsonBackReference
    private User user;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Group group = (Group) o;
        return username != null && Objects.equals(username, group.username)
                && groupId != null && Objects.equals(groupId, group.groupId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, groupId);
    }

    @Override
    protected GroupPK getPk() {
        return new GroupPK(username, groupId);
    }
}
