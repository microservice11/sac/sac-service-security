package id.co.gtx.sacservicesecurity.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DtoRoleMenu implements Serializable {
    private static final long serialVersionUID = -1135363784907116352L;

    @NotBlank(message = "Menu ID harus diisi")
    private String id;

    @NotBlank(message = "Menu Type harus diisi")
    @JsonProperty("menu_type")
    private String menuType;

    private String name;

    @JsonProperty("parent_id")
    private String parentId;

    @JsonProperty("form_app")
    private DtoMenuFormApp formApp;
}
