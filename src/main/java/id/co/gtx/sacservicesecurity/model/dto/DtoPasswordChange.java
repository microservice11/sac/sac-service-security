package id.co.gtx.sacservicesecurity.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DtoPasswordChange {

    @JsonProperty("old_password")
    @NotBlank(message = "Password Lama harus diisi")
    private String oldPassword;

    @JsonProperty("new_password")
    @NotBlank(message = "Password Baru harus diisi")
    private String newPassword;

    @JsonProperty("confirm_password")
    @NotBlank(message = "Konfirmasi Password Baru harus diisi")
    private String confirmPassword;
}
