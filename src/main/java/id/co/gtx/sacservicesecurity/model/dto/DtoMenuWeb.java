package id.co.gtx.sacservicesecurity.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DtoMenuWeb implements Serializable {
    private static final long serialVersionUID = 1477423901179546059L;

    @JsonProperty("full_name")
    private String fullName;

    private Set<DtoMenuGet> menus;
}
