package id.co.gtx.sacservicesecurity.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DtoOtp implements Serializable {
    private static final long serialVersionUID = -7067496555847103848L;

    @NotBlank(message = "Username harus diisi")
    private String username;

    @NotBlank(message = "Password harus diisi")
    private String password;

    @NotBlank(message = "Otp harus diisi")
    private String otp;
}
