package id.co.gtx.sacservicesecurity.model.entity.postgresql;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import id.co.gtx.sacservicesecurity.model.dto.AuditEntity;
import id.co.gtx.sacservicesecurity.model.pk.ResourceAccessPK;
import id.co.gtx.sacservicesecurity.service.impl.AuditingEntitySecurityListener;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

@Getter
@Setter
@Entity
@Builder
@ToString
@AllArgsConstructor
@RequiredArgsConstructor
@Table(name = "resource_access")
@IdClass(ResourceAccessPK.class)
@EntityListeners(AuditingEntitySecurityListener.class)
public class ResourceAccess extends AuditEntity<ResourceAccessPK> implements Serializable {
    private static final long serialVersionUID = 896472293876732614L;

    @Id
    @Column(length = 20)
    private String username;

    @Id
    @Column(name = "client_id", length = 100)
    @JsonProperty("client_id")
    private String clientId;

    @ManyToOne
    @JsonBackReference
    @JoinColumns({
            @JoinColumn(name = "username", referencedColumnName = "username", insertable = false, updatable = false)
    })
    private User user;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    @JoinTable(name = "resource_access_role",
            joinColumns = {@JoinColumn(name = "client_id", referencedColumnName = "client_id"),
                    @JoinColumn(name = "username", referencedColumnName = "username")},
            inverseJoinColumns = {@JoinColumn(name = "role_id", referencedColumnName = "id")}
    )
    private Set<Role> roles;

    public ResourceAccess(String username, String clientId, Set<Role> roles) {
        this.username = username;
        this.clientId = clientId;
        this.roles = roles;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        ResourceAccess that = (ResourceAccess) o;
        return username != null && Objects.equals(username, that.username)
                && clientId != null && Objects.equals(clientId, that.clientId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, clientId);
    }

    @Override
    protected ResourceAccessPK getPk() {
        return new ResourceAccessPK(username, clientId);
    }
}
