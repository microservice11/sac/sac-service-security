package id.co.gtx.sacservicesecurity.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DtoRole implements Serializable {
    private static final long serialVersionUID = -5998081062889232832L;

    @NotBlank(message = "ID harus diisi")
    private String id;

    @NotBlank(message = "Nama Role harus diisi")
    private String name;

    private Set<DtoRoleMenu> menus;

    @JsonProperty(value = "create_at", access = JsonProperty.Access.READ_ONLY)
    private LocalDateTime createAt;

    @JsonProperty(value = "update_at", access = JsonProperty.Access.READ_ONLY)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private LocalDateTime updateAt;
}
