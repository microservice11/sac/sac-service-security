package id.co.gtx.sacservicesecurity.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DtoUserActive implements Serializable {
    private static final long serialVersionUID = -5005955326495659162L;

    private String id;

    private String username;

    @JsonProperty("default_user")
    private boolean defaultUser;

    @JsonProperty("last_login")
    private LocalDateTime lastLogin;

    private String[] roles;

    @JsonProperty("ip_address")
    private String ipAddress;

    @JsonProperty("token")
    private String tokenValue;

}
