package id.co.gtx.sacservicesecurity.model.entity.postgresql;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import id.co.gtx.sacservicesecurity.model.dto.AuditEntity;
import id.co.gtx.sacservicesecurity.model.pk.MenuPK;
import id.co.gtx.sacservicesecurity.service.impl.AuditingEntitySecurityListener;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Set;

@Getter
@Setter
@Entity
@Builder
@ToString
@AllArgsConstructor
@Table(name = "menu")
@IdClass(MenuPK.class)
@RequiredArgsConstructor
@EntityListeners(AuditingEntitySecurityListener.class)
public class Menu extends AuditEntity<MenuPK> implements Serializable {
    private static final long serialVersionUID = -8777337480784555230L;

    @Id
    @Column(length = 10)
    private String id;

    @Id
    @JsonProperty("menu_type")
    @Column(name = "menu_type", length = 3, nullable = false)
    private String menuType;

    @Column(name = "name", length = 25)
    private String name;

    @JsonProperty("parent_id")
    @Column(name = "parent_id", length = 10)
    private String parentId;

    @Column(length = 100)
    private String path;

    @JsonProperty("path_parameter")
    @Column(name = "path_parameter", length = 100)
    private String pathParameter;

    private Short seq;

    @JsonProperty("form_id")
    @Column(name = "form_id", length = 10, nullable = true)
    private String formId;

    @JsonProperty("form_type")
    @Column(name = "form_type", length = 3, nullable = true)
    private String formType;

    @ManyToOne
    @JsonProperty("form_app")
    @JoinColumns({
            @JoinColumn(name = "form_id", referencedColumnName = "id", insertable = false, updatable = false),
            @JoinColumn(name = "form_type", referencedColumnName = "form_type", insertable = false, updatable = false)
    })
    private FormApp formApp;

    @JsonBackReference
    @ManyToMany(mappedBy = "menus")
    @ToString.Exclude
    private Set<Role> roles;

    @JsonProperty("create_at")
    @Column(name = "create_at", nullable = false)
    private LocalDateTime createAt;

    @JsonProperty("update_at")
    @Column(name = "update_at")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private LocalDateTime updateAt;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Menu menu = (Menu) o;
        return id != null && Objects.equals(id, menu.id)
                && menuType != null && Objects.equals(menuType, menu.menuType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, menuType);
    }

    @Override
    protected MenuPK getPk() {
        return new MenuPK(id, menuType);
    }
}
