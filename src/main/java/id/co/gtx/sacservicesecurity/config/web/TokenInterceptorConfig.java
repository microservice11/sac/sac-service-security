package id.co.gtx.sacservicesecurity.config.web;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpMethod;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.security.oauth2.core.OAuth2TokenType;
import org.springframework.security.oauth2.jwt.JwtClaimAccessor;
import org.springframework.security.oauth2.server.authorization.JdbcOAuth2AuthorizationService;
import org.springframework.security.oauth2.server.authorization.OAuth2Authorization;
import org.springframework.security.oauth2.server.authorization.client.JdbcRegisteredClientRepository;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClient;
import org.springframework.security.oauth2.server.resource.web.DefaultBearerTokenResolver;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.util.CollectionUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

@Slf4j
public class TokenInterceptorConfig implements HandlerInterceptor {

    private final ApplicationContext applicationContext;

    public TokenInterceptorConfig(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        log.info("CHECK INTERCEPTOR");
        String servletPath = request.getServletPath();
        String method = request.getMethod();
        log.info(servletPath);
        log.info(method);

        //if uri not found
        boolean passCheck = false;
        for (CustomPathMatcher customPathMatcher : pathMatcherList()) {
            AntPathRequestMatcher requestMatcher = new AntPathRequestMatcher(customPathMatcher.getPattern(), customPathMatcher.getHttpMethod());
            if (requestMatcher.matches(request)) {
                passCheck = true;
            }
        }
        for (CustomPathMatcher customPathMatcher : pathPermitList()) {
            AntPathRequestMatcher requestMatcher = new AntPathRequestMatcher(customPathMatcher.getPattern(), customPathMatcher.getHttpMethod());
            if (requestMatcher.matches(request)) {
                passCheck = true;
            }
        }
        if (!passCheck)
            return HandlerInterceptor.super.preHandle(request, response, handler);

        //cek permit uri
        boolean permitCheck = false;
        for (CustomPathMatcher customPathMatcher : pathPermitList()) {
            AntPathRequestMatcher requestMatcher = new AntPathRequestMatcher(customPathMatcher.getPattern(), customPathMatcher.getHttpMethod());
            if (customPathMatcher.getRoles().contains("PERMIT") && requestMatcher.matches(request)) {
                permitCheck = true;
            }
        }

        //cek auth uri
        boolean authCheck = false;
        for (CustomPathMatcher customPathMatcher : pathMatcherList()) {
            AntPathRequestMatcher requestMatcher = new AntPathRequestMatcher(customPathMatcher.getPattern(), customPathMatcher.getHttpMethod());
            if (requestMatcher.matches(request)) {
                authCheck = true;
            }
        }
        if (permitCheck && !authCheck)
            return HandlerInterceptor.super.preHandle(request, response, handler);



        DefaultBearerTokenResolver tokenResolver = applicationContext.getBean(DefaultBearerTokenResolver.class);
        String token = tokenResolver.resolve(request);
        if (token == null) {
            response.setStatus(401);
            return false;
        }
        JdbcOAuth2AuthorizationService authorizationService = applicationContext.getBean(JdbcOAuth2AuthorizationService.class);
        OAuth2Authorization tokenService = authorizationService.findByToken(token, OAuth2TokenType.ACCESS_TOKEN);
        if (tokenService == null) {
            response.setStatus(401);
            return false;
        }

        OAuth2Authorization.Token<OAuth2AccessToken> accessToken = tokenService.getAccessToken();
        if (!accessToken.isActive()) {
            response.setStatus(401);
            return false;
        }

        Map<String, Object> claims = accessToken.getClaims();
        if (!CollectionUtils.isEmpty(claims)) {
            JwtClaimAccessor jwtClaims = () -> claims;

            Map<String, Map<String, Set<String>>> resourceAccess = jwtClaims.getClaim("resource_access");
            if (resourceAccess == null) {
                response.setStatus(401);
                return false;
            }

            JdbcRegisteredClientRepository clientRepository = applicationContext.getBean(JdbcRegisteredClientRepository.class);
            List<RegisteredClient> clients = clientRepository.findAll();
            Collection<String> listAuthority = new HashSet<>();
            clients.forEach(client -> {
                for (String key : resourceAccess.keySet()) {
                    Map<String, Set<String>> rolesMap = resourceAccess.get(key);
                    Collection<String> roles = castAuthoritiesToCollection(rolesMap.get("roles"));
                    roles.forEach(role -> listAuthority.add("ROLE_" + role));
                    roles.forEach(role -> listAuthority.add("ROLE_" + role + "_" + key.toUpperCase()));
                }
            });

            if (!matcherUrl(request, listAuthority)) {
                response.setStatus(403);
                return false;
            }
        }
        return HandlerInterceptor.super.preHandle(request, response, handler);
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        HandlerInterceptor.super.postHandle(request, response, handler, modelAndView);
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        HandlerInterceptor.super.afterCompletion(request, response, handler, ex);
    }

    @SuppressWarnings("unchecked")
    private Collection<String> castAuthoritiesToCollection(Object authorities) {
        return (Collection<String>) authorities;
    }

    private boolean matcherUrl(HttpServletRequest request, Collection<String> listAuthority) {
        boolean match = false;
        for (CustomPathMatcher customPathMatcher : pathMatcherList()) {
            AntPathRequestMatcher requestMatcher = new AntPathRequestMatcher(customPathMatcher.getPattern(), customPathMatcher.getHttpMethod());
            if ((customPathMatcher.getRoles().contains("PERMIT") || customPathMatcher.getRoles().contains("AUTH")) && requestMatcher.matches(request)) {
                match = true;
            } else {
                for (String role : listAuthority) {
                    if (customPathMatcher.getRoles().contains(role) && requestMatcher.matches(request)) {
                        match = true;
                    }
                }
            }
        }

        return match;
    }

    private List<CustomPathMatcher> pathPermitList() {
        List<CustomPathMatcher> customPathMatchers = new ArrayList<>();
        for (HttpMethod httpMethod : HttpMethod.values()) {
            customPathMatchers.add(new CustomPathMatcher("/error", httpMethod, new String[]{"PERMIT"}));
        }

        customPathMatchers.add(new CustomPathMatcher("/uaa/auth/**", HttpMethod.POST, new String[]{"PERMIT"}));

        return customPathMatchers;
    }

    private List<CustomPathMatcher> pathMatcherList() {
        List<CustomPathMatcher> customPathMatchers = new ArrayList<>();

        customPathMatchers.add(new CustomPathMatcher("/uaa/auth/active", HttpMethod.GET, new String[]{"AUTH"}));
        customPathMatchers.add(new CustomPathMatcher("/uaa/auth/logout", HttpMethod.POST, new String[]{"AUTH"}));
        customPathMatchers.add(new CustomPathMatcher("/uaa/auth/terminate/**", HttpMethod.DELETE, new String[]{"ROLE_ADMIN_WEB", "ROLE_SUPER_ADMIN_WEB"}));

        customPathMatchers.add(new CustomPathMatcher("/uaa/form/**", HttpMethod.GET, new String[]{"AUTH"}));
        customPathMatchers.add(new CustomPathMatcher("/uaa/form", HttpMethod.POST, new String[]{"ROLE_ADMIN_WEB", "ROLE_SUPER_ADMIN_WEB"}));
        customPathMatchers.add(new CustomPathMatcher("/uaa/form", HttpMethod.PUT, new String[]{"ROLE_ADMIN_WEB", "ROLE_SUPER_ADMIN_WEB"}));
        customPathMatchers.add(new CustomPathMatcher("/uaa/form/**", HttpMethod.DELETE, new String[]{"ROLE_ADMIN_WEB", "ROLE_SUPER_ADMIN_WEB"}));

        customPathMatchers.add(new CustomPathMatcher("/uaa/menu/**", HttpMethod.GET, new String[]{"AUTH"}));
        customPathMatchers.add(new CustomPathMatcher("/uaa/menu", HttpMethod.POST, new String[]{"ROLE_ADMIN_WEB", "ROLE_SUPER_ADMIN_WEB"}));
        customPathMatchers.add(new CustomPathMatcher("/uaa/menu", HttpMethod.PUT, new String[]{"ROLE_ADMIN_WEB", "ROLE_SUPER_ADMIN_WEB"}));
        customPathMatchers.add(new CustomPathMatcher("/uaa/menu/**", HttpMethod.DELETE, new String[]{"ROLE_ADMIN_WEB", "ROLE_SUPER_ADMIN_WEB"}));

        customPathMatchers.add(new CustomPathMatcher("/uaa/user/**", HttpMethod.GET, new String[]{"AUTH"}));
        customPathMatchers.add(new CustomPathMatcher("/uaa/user/**", HttpMethod.POST, new String[]{"ROLE_ADMIN_WEB", "ROLE_SUPER_ADMIN_WEB"}));
        customPathMatchers.add(new CustomPathMatcher("/uaa/user", HttpMethod.PUT, new String[]{"ROLE_ADMIN_WEB", "ROLE_SUPER_ADMIN_WEB"}));
        customPathMatchers.add(new CustomPathMatcher("/uaa/user", HttpMethod.DELETE, new String[]{"ROLE_ADMIN_WEB", "ROLE_SUPER_ADMIN_WEB"}));

        customPathMatchers.add(new CustomPathMatcher("/uaa/role/**", HttpMethod.GET, new String[]{"AUTH"}));
        customPathMatchers.add(new CustomPathMatcher("/uaa/role/**", HttpMethod.POST, new String[]{"ROLE_SUPER_ADMIN_WEB", "ROLE_SUPER_ADMIN"}));
        customPathMatchers.add(new CustomPathMatcher("/uaa/role", HttpMethod.PUT, new String[]{"ROLE_SUPER_ADMIN_WEB", "ROLE_SUPER_ADMIN"}));
        customPathMatchers.add(new CustomPathMatcher("/uaa/role", HttpMethod.DELETE, new String[]{"ROLE_SUPER_ADMIN_WEB", "ROLE_SUPER_ADMIN"}));

        return customPathMatchers;
    }
}
