package id.co.gtx.sacservicesecurity.config.security.jackson2;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.module.SimpleModule;
import id.co.gtx.sacmodules.dto.DtoGroup;
import id.co.gtx.sacservicesecurity.model.dto.DtoUserDetails;
import org.springframework.security.jackson2.SecurityJackson2Modules;

@SuppressWarnings("serial")
public class SacSecurityJackson2Modul extends SimpleModule {

    public SacSecurityJackson2Modul() {
        super(SacSecurityJackson2Modul.class.getName(), new Version(1, 0, 0, null, null, null));
    }

    @Override
    public void setupModule(SetupContext context) {
        SecurityJackson2Modules.enableDefaultTyping(context.getOwner());
        context.setMixInAnnotations(DtoUserDetails.class, DtoUserDetailsMixin.class);
        context.setMixInAnnotations(DtoGroup.class, DtoGroupMixin.class);
    }
}
