package id.co.gtx.sacservicesecurity.config.jpa;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaAuditing
@EnableJpaRepositories(basePackages = {"id.co.gtx.sacservicesecurity.repository.postgresql"})
public class JpaConfig {
}
