package id.co.gtx.sacservicesecurity.config.security.jackson2;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.MissingNode;
import id.co.gtx.sacmodules.dto.DtoGroup;

import java.io.IOException;

class DtoGroupDeserializer extends JsonDeserializer<DtoGroup> {

    @Override
    public DtoGroup deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        ObjectMapper mapper = (ObjectMapper) jp.getCodec();
        JsonNode jsonNode = mapper.readTree(jp);
        String groupId = readJsonNode(jsonNode, "group_id").asText();
        Integer type = readJsonNode(jsonNode, "type").asInt();
        DtoGroup result = new DtoGroup(groupId, type);
        return result;
    }

    private JsonNode readJsonNode(JsonNode jsonNode, String field) {
        return jsonNode.has(field) ? jsonNode.get(field) : MissingNode.getInstance();
    }
}
