package id.co.gtx.sacservicesecurity.config.web;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpMethod;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@NoArgsConstructor
public class CustomPathMatcher {

    private String pattern;
    private String httpMethod;
    private Set<String> roles;

    public CustomPathMatcher(String pattern, HttpMethod httpMethod, String[] roles) {
        this.pattern = pattern;
        this.httpMethod = httpMethod.name();
        this.roles = Arrays.stream(roles).collect(Collectors.toSet());
    }
}
