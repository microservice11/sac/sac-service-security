package id.co.gtx.sacservicesecurity.config.redis;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;

@Configuration
@EnableRedisRepositories(basePackages = {"id.co.gtx.sacservicesecurity.repository.redis"})
public class RedisConfig {
}
