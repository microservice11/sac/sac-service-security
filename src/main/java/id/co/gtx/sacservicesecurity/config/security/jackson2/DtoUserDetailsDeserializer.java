package id.co.gtx.sacservicesecurity.config.security.jackson2;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.MissingNode;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import id.co.gtx.sacmodules.dto.DtoGroup;
import id.co.gtx.sacservicesecurity.model.dto.DtoUserDetails;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Set;

class DtoUserDetailsDeserializer extends JsonDeserializer<DtoUserDetails> {

    private static final TypeReference<Set<SimpleGrantedAuthority>> SIMPLE_GRANTED_AUTHORITY_SET = new TypeReference<Set<SimpleGrantedAuthority>>() {
    };
    private static final TypeReference<Set<DtoGroup>> GROUP_SET = new TypeReference<Set<DtoGroup>>() {
    };

    @Override
    public DtoUserDetails deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        ObjectMapper mapper = (ObjectMapper) jp.getCodec();
        mapper.registerModule(new JavaTimeModule());
        JsonNode jsonNode = mapper.readTree(jp);
        Set<? extends GrantedAuthority> authorities = mapper.convertValue(jsonNode.get("authorities"),
                SIMPLE_GRANTED_AUTHORITY_SET);
        Set<? extends DtoGroup> groups = mapper.convertValue(jsonNode.get("groups"), GROUP_SET);
        JsonNode passwordNode = readJsonNode(jsonNode, "password");
        String username = readJsonNode(jsonNode, "username").asText();
        String password = passwordNode.asText("");
        boolean enabled = readJsonNode(jsonNode, "enabled").asBoolean();
        boolean accountNonExpired = readJsonNode(jsonNode, "accountNonExpired").asBoolean();
        boolean credentialsNonExpired = readJsonNode(jsonNode, "credentialsNonExpired").asBoolean();
        boolean accountNonLocked = readJsonNode(jsonNode, "accountNonLocked").asBoolean();
        boolean defaultUser = readJsonNode(jsonNode, "defaultUser").asBoolean();
        LocalDateTime lastLogin = mapper.convertValue(readJsonNode(jsonNode, "lastLogin"), LocalDateTime.class);
        return new DtoUserDetails(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked,
                authorities, groups, defaultUser, lastLogin);
    }

    private JsonNode readJsonNode(JsonNode jsonNode, String field) {
        return jsonNode.has(field) ? jsonNode.get(field) : MissingNode.getInstance();
    }
}
