package id.co.gtx.sacservicesecurity.service;

import id.co.gtx.sacmodules.dto.DtoResponse;
import id.co.gtx.sacservicesecurity.model.dto.DtoGroup;

import java.util.List;

public interface GroupService {
    DtoResponse<List<DtoGroup>> findByUsername(String username);
}
