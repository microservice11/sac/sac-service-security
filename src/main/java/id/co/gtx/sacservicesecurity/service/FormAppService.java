package id.co.gtx.sacservicesecurity.service;

import id.co.gtx.sacmodules.dto.DtoResponse;
import id.co.gtx.sacservicesecurity.model.dto.DtoFormApp;

import java.util.List;

public interface FormAppService {
    DtoResponse<List<DtoFormApp>> findAll();

    DtoResponse<DtoFormApp> findByIdAndType(String id, String type);

    DtoResponse<DtoFormApp> createForm(DtoFormApp dtoFormApp);

    DtoResponse<DtoFormApp> updateForm(DtoFormApp dtoFormApp);

    DtoResponse<DtoFormApp> deleteForm(String id, String type);
}
