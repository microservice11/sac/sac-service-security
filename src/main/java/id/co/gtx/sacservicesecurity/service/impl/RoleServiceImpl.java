package id.co.gtx.sacservicesecurity.service.impl;

import id.co.gtx.sacmodules.dto.DtoError;
import id.co.gtx.sacmodules.dto.DtoResponse;
import id.co.gtx.sacmodules.dto.ErrorCode;
import id.co.gtx.sacmodules.dto.StatusCode;
import id.co.gtx.sacmodules.enumz.Status;
import id.co.gtx.sacmodules.exception.OperationException;
import id.co.gtx.sacmodules.service.HeadersService;
import id.co.gtx.sacmodules.service.ServiceErrorHandling;
import id.co.gtx.sacmodules.utils.MapperUtil;
import id.co.gtx.sacservicesecurity.model.dto.DtoRole;
import id.co.gtx.sacservicesecurity.model.entity.postgresql.Role;
import id.co.gtx.sacservicesecurity.repository.postgresql.RoleRepository;
import id.co.gtx.sacservicesecurity.service.RoleService;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {

    private final HeadersService headersService;
    private final MapperUtil mapperUtil;
    private final RoleRepository roleRepository;
    private final ServiceErrorHandling serviceErrorHandling;

    public RoleServiceImpl(HeadersService headersService,
                           MapperUtil mapperUtil,
                           RoleRepository roleRepository,
                           ServiceErrorHandling serviceErrorHandling) {
        this.headersService = headersService;
        this.mapperUtil = mapperUtil;
        this.roleRepository = roleRepository;
        this.serviceErrorHandling = serviceErrorHandling;
    }

    @Override
    @Transactional(readOnly = true)
    public DtoResponse<List<DtoRole>> findAll() {
        List<String> roleNames = new ArrayList<>(headersService.getBaseRoles());
        List<Role> roles = roleRepository.findByNameIn(roleNames);
        if (roles.isEmpty())
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED_NOT_FOUND, Status.FAILED, "Gagal menampilkan data Role"));
        return new DtoResponse<>(StatusCode.SUCCESS, Status.SUCCESS, "Berhasil menampilkan data Role", mapperUtil.mappingClass(roles, DtoRole.class));
    }

    @Override
    @Transactional(readOnly = true)
    public DtoResponse<DtoRole> findById(String id) {
        if ("".equals(id) || id == null)
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "Path Parameter Role ID cannot null"));

        Role role = roleRepository.findById(id).orElseThrow(() -> {
            DtoError error = new DtoError(ErrorCode.ID_TAKEN, "Role ID " + id + " tidak ditemukan");
            return new OperationException(new DtoResponse<>(StatusCode.FAILED_NOT_FOUND, Status.FAILED, "Gagal menampilkan data Role", null, error));
        });

        return new DtoResponse<>(StatusCode.SUCCESS, Status.SUCCESS, "Berhasil menampilkan Role By ID " + id, mapperUtil.mappingClass(role, DtoRole.class));
    }

    @Transactional(rollbackFor = {DataAccessException.class})
    public DtoResponse<DtoRole> createRole(DtoRole dtoRole) {

        serviceErrorHandling.validateSchema(dtoRole).ifPresent(error -> {
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED_CREATE, Status.FAILED, "Gagal menambakan data Role baru", null, error));
        });

        if (dtoRole.getName().contains("ROLE")) {
            DtoError error = new DtoError(ErrorCode.ID_TAKEN, "Data inputan tidak valid", new ArrayList<>());
            error.addError("Field", "name", "Nama tidak boleh mengandung kata ROLE", null, null, dtoRole.getName());
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "Gagal menambakan data \"ROLE\" baru", null, error));
        }

        roleRepository.findById(dtoRole.getId()).ifPresent(role -> {
            DtoError error = new DtoError(ErrorCode.ID_TAKEN, "Data inputan sudah ada", new ArrayList<>());
            error.addError("Field", "id", "Role ID sudah ada", null, null, role.getId());
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "Gagal menambakan data Role baru", null, error));
        });

        Role role = mapperUtil.mappingClass(dtoRole, Role.class);
        role.setCreateAt(LocalDateTime.now());

        try {
            Role roleSaved = roleRepository.save(role);
            return new DtoResponse<>(StatusCode.SUCCESS_CREATE, Status.SUCCESS, "Berhasil menambahkan data Role baru", mapperUtil.mappingClass(roleSaved, DtoRole.class));
        } catch (DataAccessException e) {
            throw serviceErrorHandling.handleDataAccessException(e, StatusCode.FAILED_CREATE, Status.FAILED, "Gagal menambahkan data Role baru");
        }
    }

    @Override
    @Transactional(rollbackFor = {DataAccessException.class})
    public DtoResponse<DtoRole> updateRole(DtoRole dtoRole) {
        serviceErrorHandling.validateSchema(dtoRole).ifPresent(error -> {
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED_UPDATE, Status.FAILED, "Gagal update data Role", null, error));
        });

        Role role = roleRepository.findById(dtoRole.getId()).orElseThrow(() -> {
            DtoError error = new DtoError(ErrorCode.ID_TAKEN, "Data inputan tidak ditemukan", new ArrayList<>());
            error.addError("Field", "id", "Role ID tidak ditemukaan");
            return new OperationException(new DtoResponse<>(StatusCode.FAILED_UPDATE, Status.FAILED, "Gagal update data Role", null, error));
        });

        Role roleUpdate = mapperUtil.mappingClass(dtoRole, Role.class);
        roleUpdate.setCreateAt(role.getCreateAt());
        roleUpdate.setUpdateAt(LocalDateTime.now());

        try {
            Role roleSaved = roleRepository.save(roleUpdate);
            return new DtoResponse<>(StatusCode.SUCCESS_UPDATE, Status.SUCCESS, "Berhasil update data Role", mapperUtil.mappingClass(roleSaved, DtoRole.class));
        } catch (DataAccessException e) {
            throw serviceErrorHandling.handleDataAccessException(e, StatusCode.FAILED_UPDATE, Status.FAILED, "Gagal update data Role");
        }
    }

    @Override
    @Transactional(rollbackFor = {DataAccessException.class})
    public DtoResponse<DtoRole> deleteRole(String id) {
        Role role = roleRepository.findById(id).orElseThrow(() -> {
            DtoError error = new DtoError(ErrorCode.ID_TAKEN, "Data inputan tidak ditemukan", new ArrayList<>());
            error.addError("Field", "id", "Role ID tidak ditemukaan");
            return new OperationException(new DtoResponse<>(StatusCode.FAILED_DELETE, Status.FAILED, "Gagal hapus data Role", null, error));
        });

        if (!role.getResourceAccesses().isEmpty()) {
            DtoError error = new DtoError(ErrorCode.ID_TAKEN, "Role ID " + id + " sudah ter-binding pada Resource Access");
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED_DELETE, Status.FAILED, "Gagal hapus data Role", null, error));
        }

        try {
            roleRepository.delete(role);
            return new DtoResponse<>(StatusCode.SUCCESS_DELETE, Status.SUCCESS, "Berhasil hapus data Role", mapperUtil.mappingClass(role, DtoRole.class));
        } catch (DataAccessException e) {
            throw serviceErrorHandling.handleDataAccessException(e, StatusCode.FAILED_DELETE, Status.FAILED, "Gagal hapus data Role");
        }
    }
}
