package id.co.gtx.sacservicesecurity.service;

import id.co.gtx.sacmodules.dto.DtoResponse;
import id.co.gtx.sacservicesecurity.model.dto.DtoRole;

import java.util.List;

public interface RoleService {
    DtoResponse<List<DtoRole>> findAll();

    DtoResponse<DtoRole> findById(String id);

    DtoResponse<DtoRole> createRole(DtoRole dtoRole);

    DtoResponse<DtoRole> updateRole(DtoRole dtoRole);

    DtoResponse<DtoRole> deleteRole(String id);
}
