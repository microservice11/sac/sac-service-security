package id.co.gtx.sacservicesecurity.service.impl;

import id.co.gtx.sacmodules.dto.*;
import id.co.gtx.sacmodules.enumz.Status;
import id.co.gtx.sacmodules.exception.OperationException;
import id.co.gtx.sacmodules.service.HeadersService;
import id.co.gtx.sacmodules.service.ServiceErrorHandling;
import id.co.gtx.sacmodules.utils.MapperUtil;
import id.co.gtx.sacservicesecurity.model.dto.DtoMenu;
import id.co.gtx.sacservicesecurity.model.dto.DtoMenuGet;
import id.co.gtx.sacservicesecurity.model.dto.DtoMenuWeb;
import id.co.gtx.sacservicesecurity.model.entity.postgresql.Menu;
import id.co.gtx.sacservicesecurity.model.entity.postgresql.Role;
import id.co.gtx.sacservicesecurity.model.pk.MenuPK;
import id.co.gtx.sacservicesecurity.repository.postgresql.MenuRepository;
import id.co.gtx.sacservicesecurity.service.MenuService;
import id.co.gtx.sacservicesecurity.service.UserService;
import org.springframework.dao.DataAccessException;
import org.springframework.security.oauth2.server.authorization.client.JdbcRegisteredClientRepository;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClient;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClientRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
public class MenuServiceImpl implements MenuService {

    private final HeadersService headersService;
    private final MapperUtil mapperUtil;
    private final MenuRepository menuRepository;
    private final UserService userService;
    private final RegisteredClientRepository registeredClientRepository;
    private final ServiceErrorHandling serviceErrorHandling;

    public MenuServiceImpl(HeadersService headersService,
                           MapperUtil mapperUtil,
                           MenuRepository menuRepository,
                           UserService userService,
                           RegisteredClientRepository registeredClientRepository,
                           ServiceErrorHandling serviceErrorHandling) {
        this.headersService = headersService;
        this.mapperUtil = mapperUtil;
        this.menuRepository = menuRepository;
        this.userService = userService;
        this.registeredClientRepository = registeredClientRepository;
        this.serviceErrorHandling = serviceErrorHandling;
    }

    @Override
    @Transactional(readOnly = true)
    public DtoResponse<List<DtoMenu>> findAll() {
        List<Menu> menus = menuRepository.findAll();
        if (menus.isEmpty())
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED_NOT_FOUND, Status.FAILED, "Gagal menampilkan data Menu"));

        return new DtoResponse<>(StatusCode.SUCCESS, Status.SUCCESS, "Berhasil menampilkan data Menu", mapperUtil.mappingClass(menus, DtoMenu.class));
    }

    @Override
    @Transactional(readOnly = true)
    public DtoResponse<DtoMenu> findByIdAndType(String id, String type) {
        return new DtoResponse<>(StatusCode.SUCCESS, Status.SUCCESS, "Berhasil menampilkan data Menu By ID " + id + " dan Type " + type,
                mapperUtil.mappingClass(findByPk(id, type), DtoMenu.class));
    }

    @Override
    @Transactional(readOnly = true)
    public DtoResponse<List<DtoMenu>> findByParent() {
        List<Menu> menus = menuRepository.findByParentIdIsNullAndFormAppIsNull();
        if (menus.isEmpty())
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED_NOT_FOUND, Status.FAILED, "Menu Parent ID tidak ditemukan"));

        return new DtoResponse<>(StatusCode.SUCCESS, Status.SUCCESS, "Berhasil menampilkan data Menu Parent ID", mapperUtil.mappingClass(menus, DtoMenu.class));
    }

    @Override
    @Transactional(readOnly = true)
    public DtoResponse<List<DtoMenu>> findByParentId(String parentId) {
        if ("".equals(parentId) || parentId == null)
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "Path Parameter ParentId cannot null"));

        List<Menu> menus = menuRepository.findByParentId(parentId);
        if (menus.isEmpty()) {
            DtoError error = new DtoError(ErrorCode.ID_TAKEN, "ParentId " + parentId + " tidak ditemukan");
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED_NOT_FOUND, Status.FAILED, "Gagal menampilkan data Menu", null, error));
        }

        return new DtoResponse<>(StatusCode.SUCCESS, Status.SUCCESS, "Berhasil menampilkan data Menu By ParentId " + parentId, mapperUtil.mappingClass(menus, DtoMenu.class));
    }

    @Override
    @Transactional(readOnly = true)
    public DtoResponse<DtoMenuWeb> findMenuWeb() {
        String fullName = userService.findNameById(headersService.getUsername()).getData().getFullName();

        Map<String, Map<String, Set<String>>> resourceAccess = headersService.getResourceAccess();
        if (resourceAccess.get("web") == null)
            throw new OperationException(
                    new DtoResponse<>(StatusCode.FAILED_NOT_FOUND, Status.FAILED, "Username " + headersService.getUsername() + " Tidak diizinkan Akses WEB"));

        Map<String, Set<String>> web = resourceAccess.get("web");
        Set<String> roles = web.get("roles");
        Set<Menu> menus = menuRepository.findByRoleName(new ArrayList<>(roles));
        if (menus.isEmpty())
            throw new OperationException(
                    new DtoResponse<>(StatusCode.FAILED_NOT_FOUND, Status.FAILED, "Daftar Menu tidak ditemukan"));

        DtoMenuWeb dtoMenuWeb = new DtoMenuWeb(fullName, mapperUtil.mappingClass(menus, DtoMenuGet.class));

        return new DtoResponse<>(StatusCode.SUCCESS, Status.SUCCESS, "Daftar Menu berhasil ditemukan", dtoMenuWeb);
    }

    @Override
    @Transactional(readOnly = true)
    public DtoResponse<List<String>> getClientId() {
        List<RegisteredClient> registeredClients = ((JdbcRegisteredClientRepository) registeredClientRepository).findAll();
        if (registeredClients.isEmpty())
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "Daftar Client ID tidak ada"));

        List<String> clientId = new ArrayList<>();
        registeredClients.forEach(registeredClient -> clientId.add(registeredClient.getClientId()));
        return new DtoResponse<>(StatusCode.SUCCESS, Status.SUCCESS, "", clientId);
    }

    @Override
    @Transactional(rollbackFor = {DataAccessException.class})
    public DtoResponse<DtoMenu> createMenu(DtoMenu dtoMenu) {
        dtoMenu.setId("0");
        serviceErrorHandling.validateSchema(dtoMenu).ifPresent(error -> {
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED_CREATE, Status.FAILED, "Gagal menambakan data Menu baru", null, error));
        });

        validation(dtoMenu);

        Integer idMax = menuRepository.findByIdMax();
        String id = "0000000000" + idMax;
        dtoMenu.setId(id.substring(id.length() - 10));

        Menu menu = mapperUtil.mappingClass(dtoMenu, Menu.class);
        menu.setCreateAt(LocalDateTime.now());

        try {
            Menu menuSaved = menuRepository.save(menu);
            return new DtoResponse<>(StatusCode.SUCCESS_CREATE, Status.SUCCESS, "Berhasil menambakan data Menu baru", mapperUtil.mappingClass(menuSaved, DtoMenu.class));
        } catch (DataAccessException e) {
            throw serviceErrorHandling.handleDataAccessException(e, StatusCode.FAILED_CREATE, Status.FAILED, "Gagal menambakan data Menu baru");
        }
    }

    @Override
    @Transactional(rollbackFor = {DataAccessException.class})
    public DtoResponse<DtoMenu> updateMenu(DtoMenu dtoMenu) {
        serviceErrorHandling.validateSchema(dtoMenu).ifPresent(error -> {
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED_UPDATE, Status.FAILED, "Gagal update data Menu", null, error));
        });

        validation(dtoMenu);

        Menu menu = menuRepository.findById(new MenuPK(dtoMenu.getId(), dtoMenu.getMenuType())).orElseThrow(() -> {
            DtoError error = new DtoError(ErrorCode.INVALID_PAYLOAD, "Data inputan tidak ditemukan", new ArrayList<>());
            error.addError("Field", "id", "Menu ID harus diisi");
            error.addError("Field", "menu_type", "Menu Type harus diisi");
            return new OperationException(new DtoResponse<>(StatusCode.FAILED_UPDATE, Status.FAILED, "Gagal update data Menu", null, error));
        });

        Menu menuUpdate = mapperUtil.mappingClass(dtoMenu, Menu.class);
        menuUpdate.setCreateAt(menu.getCreateAt());
        menuUpdate.setUpdateAt(LocalDateTime.now());

        try {
            return new DtoResponse<>(StatusCode.SUCCESS_UPDATE, Status.SUCCESS, "Berhasil update data Menu", mapperUtil.mappingClass(menuRepository.save(menuUpdate), DtoMenu.class));
        } catch (DataAccessException e) {
            throw serviceErrorHandling.handleDataAccessException(e, StatusCode.FAILED_UPDATE, Status.FAILED, "Gagal update data Menu");
        }
    }

    @Override
    @Transactional(rollbackFor = {DataAccessException.class})
    public DtoResponse<DtoMenu> deleteMenu(String id, String type) {
        Menu menu = findByPk(id, type);

        if (!menu.getRoles().isEmpty()) {
            String nameRole = "";
            for (Role role : menu.getRoles()) {
                nameRole += ", " + role.getName();
            }
            DtoError error = new DtoError(ErrorCode.DATA_ACCESS_EXCEPTION, "Menu ID " + id + " dengan Type " + type + " sudah terbinding pada data Role " + nameRole.substring(2));
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED_DELETE, Status.FAILED, "Gagal hapus data Menu", null, error));
        }

        try {
            menuRepository.delete(menu);
            return new DtoResponse<>(StatusCode.SUCCESS_CREATE, Status.SUCCESS, "Berhasil hapus data Menu", mapperUtil.mappingClass(menu, DtoMenu.class));
        } catch (DataAccessException e) {
            throw serviceErrorHandling.handleDataAccessException(e, StatusCode.FAILED_DELETE, Status.FAILED, "Gagal hapus data Menu");
        }
    }

    private Menu findByPk(String id, String type) {
        if ("".equals(id) || id == null)
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "Path parameter Menu ID harus diisi"));

        if ("".equals(type) || type == null)
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "Path parameter Menu Type harus diisi"));

        return menuRepository.findById(new MenuPK(id, type)).orElseThrow(() ->
                new OperationException(new DtoResponse<>(StatusCode.FAILED_NOT_FOUND, Status.FAILED, "Menu tidak ditemukan By ID " + id + " dan Type " + type))
        );
    }

    private void validation(DtoMenu dtoMenu) {
        if (dtoMenu.getFormApp() != null) {
            String formId = dtoMenu.getFormApp().getId();
            String formType = dtoMenu.getFormApp().getFormType();
            if (("".equals(dtoMenu.getParentId()) || dtoMenu.getParentId() == null)
                    && (!"".equals(formId) && formId != null)
                    && (!"".equals(formType) && formType != null)) {
                DtoError error = new DtoError(ErrorCode.INVALID_PAYLOAD, "Data inputan tidak valid", new ArrayList<>());
                error.addError("Field", "parent_id", "Menu Parent ID harus diisi");
                throw new OperationException(new DtoResponse<>(StatusCode.FAILED_CREATE, Status.FAILED, "Gagal menambahkan/update data Menu", null, error));
            } else if ((!"".equals(dtoMenu.getParentId()) && dtoMenu.getParentId() != null) && ("".equals(formId) || formId == null) && ("".equals(formType) || formType == null)){
                DtoError error = new DtoError(ErrorCode.INVALID_PAYLOAD, "Data inputan tidak valid", new ArrayList<>());
                error.addError("Field", "form_app", "Form App harus diisi");
                throw new OperationException(new DtoResponse<>(StatusCode.FAILED_CREATE, Status.FAILED, "Gagal menambahkan/update data Menu", null, error));
            } else if ((!"".equals(dtoMenu.getParentId()) && dtoMenu.getParentId() != null) && ("".equals(formId) || formId == null)){
                DtoError error = new DtoError(ErrorCode.INVALID_PAYLOAD, "Data inputan tidak valid", new ArrayList<>());
                error.addError("Field", "form_app", "Form ID harus diisi");
                throw new OperationException(new DtoResponse<>(StatusCode.FAILED_CREATE, Status.FAILED, "Gagal menambahkan/update data Menu", null, error));
            } else if ((!"".equals(dtoMenu.getParentId()) && dtoMenu.getParentId() != null) && ("".equals(formType) || formType == null)){
                DtoError error = new DtoError(ErrorCode.INVALID_PAYLOAD, "Data inputan tidak valid", new ArrayList<>());
                error.addError("Field", "form_app", "Form Type harus diisi");
                throw new OperationException(new DtoResponse<>(StatusCode.FAILED_CREATE, Status.FAILED, "Gagal menambahkan/update data Menu", null, error));
            }
        } else {
            if (!"".equals(dtoMenu.getParentId()) && dtoMenu.getParentId() != null) {
                DtoError error = new DtoError(ErrorCode.INVALID_PAYLOAD, "Data inputan tidak valid", new ArrayList<>());
                error.addError("Field", "parent_id", "Menu Parent ID tidak boleh diisi");
                throw new OperationException(new DtoResponse<>(StatusCode.FAILED_CREATE, Status.FAILED, "Gagal menambahkan/update data Menu", null, error));
            }
        }
    }
}
