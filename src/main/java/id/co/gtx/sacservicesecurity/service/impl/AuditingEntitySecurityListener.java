package id.co.gtx.sacservicesecurity.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import id.co.gtx.sacmodules.dto.DtoLog;
import id.co.gtx.sacmodules.dto.DtoLogDetail;
import id.co.gtx.sacmodules.service.HeadersService;
import id.co.gtx.sacservicesecurity.model.dto.AuditEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.util.Assert;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Slf4j
@Configurable
public class AuditingEntitySecurityListener {

    private HeadersService headersService;
    private JdbcTemplate jt;
    private StreamBridge streamBridge;

    @Autowired
    public void setHeadersService(HeadersService headersService) {
        this.headersService = headersService;
    }

    @Autowired
    public void setJt(JdbcTemplate jt) {
        this.jt = jt;
    }

    @Autowired
    public void setStreamBridge(StreamBridge streamBridge) {
        this.streamBridge = streamBridge;
    }

    @PrePersist
    public void touchForCreate(Object object) throws JsonProcessingException {
        Assert.notNull(object, "Entity must not be null!");
        AuditEntity audit = (AuditEntity) object;

        List<DtoLogDetail> logDetails = new ArrayList<>();
        Map<String, Object> newData = audit.getMapData();
        for (Map.Entry<String, Object> entry : newData.entrySet()) {
            if (!(entry.getValue() instanceof Map)
                    || !(entry.getValue() instanceof List)
                    || !(entry.getValue() instanceof Set)) {
                DtoLogDetail logDetail = DtoLogDetail.builder()
                        .colname(entry.getKey())
                        .newValue("" + entry.getValue())
                        .build();
                logDetails.add(logDetail);
            }
        }

        DtoLog dtoLog = DtoLog.builder()
                .operation("INSERT")
                .message("Menambahkan Data " + audit.getTableName())
                .tbname(audit.getTableName())
                .tbkey(audit.getStringPk())
                .username(headersService.getUsername())
                .ipClient(headersService.getIpClient())
                .createAt(LocalDateTime.now())
                .logsDetails(logDetails)
                .build();

        streamBridge.send("logData-out-0", dtoLog);
    }

    @PreUpdate
    public void touchForUpdate(Object object) {
        Assert.notNull(object, "Entity must not be null!");
        AuditEntity audit = (AuditEntity) object;
        String tableName = audit.getTableName();
        String pk = audit.getStringPk();

        String[] pkSplits = pk.split(",");
        String column = "";
        List<Object> param = new ArrayList<>();
        for (String pkSplit : pkSplits) {
            String[] split = pkSplit.trim().split("=");
            column += " and " + split[0] + " = ?";
            param.add(split[1]);
        }

        Map<String, Object> dataNew = audit.getMapData();
        List<Map<String, Object>> result = jt.query("select * from " + tableName + " where " + column.substring(5), param.toArray(), (rs, rowNum) -> {
            Map<String, Object> map = new HashMap<>();
            int rowCount = rs.getMetaData().getColumnCount();
            for (int i = 1; i <= rowCount; i++) {
                if (rs.getObject(i) instanceof Timestamp) {
                    map.put(rs.getMetaData().getColumnLabel(i), ((Timestamp) rs.getObject(i)).toLocalDateTime());
                } else if (rs.getObject(i) instanceof Date) {
                    map.put(rs.getMetaData().getColumnLabel(i), ((Date) rs.getObject(i)).toLocalDate());
                } else {
                    map.put(rs.getMetaData().getColumnLabel(i), rs.getObject(i));
                }
            }
            return map;
        });

        if (result.isEmpty()) {
            return;
        }

        Map<String, Object> dataOld = result.get(0);
        List<DtoLogDetail> logDetails = new ArrayList<>();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS");
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        for (Map.Entry<String, Object> entry : dataOld.entrySet()) {
            String key = entry.getKey();
            Object oldValue = dataOld.get(key);
            Object newValue = dataNew.get(key);

            if (oldValue instanceof LocalDateTime) {
                oldValue = ((LocalDateTime) oldValue).format(dateTimeFormatter);
                if (!oldValue.equals(newValue)) {
                    DtoLogDetail logDetail = DtoLogDetail.builder()
                            .colname(key)
                            .newValue("" + newValue)
                            .oldValue("" + oldValue)
                            .build();
                    logDetails.add(logDetail);
                }
            } else if (oldValue instanceof LocalDate) {
                oldValue = ((LocalDate) oldValue).format(dateFormatter);
                if (!oldValue.equals(newValue)) {
                    DtoLogDetail logDetail = DtoLogDetail.builder()
                            .colname(key)
                            .newValue("" + newValue)
                            .oldValue("" + oldValue)
                            .build();
                    logDetails.add(logDetail);
                }
            } else if (oldValue instanceof Integer) {
                oldValue = ((Integer) oldValue).toString();

                if (newValue instanceof Short)
                    newValue = ((Short) newValue).toString();
                else
                    newValue = ((Integer) newValue).toString();

                if (!oldValue.equals(newValue)) {
                    DtoLogDetail logDetail = DtoLogDetail.builder()
                            .colname(key)
                            .newValue("" + newValue)
                            .oldValue("" + oldValue)
                            .build();
                    logDetails.add(logDetail);
                }
            } else {
                if (oldValue != null) {
                    if (!oldValue.equals(newValue)) {
                        DtoLogDetail logDetail = DtoLogDetail.builder()
                                .colname(key)
                                .newValue("" + newValue)
                                .oldValue("" + oldValue)
                                .build();
                        logDetails.add(logDetail);
                    }
                } else if (newValue != null){
                    DtoLogDetail logDetail = DtoLogDetail.builder()
                            .colname(key)
                            .newValue("" + newValue)
                            .build();
                    logDetails.add(logDetail);
                }
            }
        }

        DtoLog dtoLog = DtoLog.builder()
                .operation("UPDATE")
                .message("Update Data " + tableName)
                .tbname(tableName)
                .tbkey(pk)
                .username(headersService.getUsername())
                .ipClient(headersService.getIpClient())
                .createAt(LocalDateTime.now())
                .logsDetails(logDetails)
                .build();

        streamBridge.send("logData-out-0", dtoLog);
    }

    @PostRemove
    public void touchForDelete(Object object) {
        Assert.notNull(object, "Entity must not be null!");
        AuditEntity audit = (AuditEntity) object;
        String tableName = audit.getTableName();
        String pk = audit.getStringPk();

        List<DtoLogDetail> logDetails = new ArrayList<>();
        Map<String, Object> newData = audit.getMapPk();
        for (Map.Entry<String, Object> entry : newData.entrySet()) {
            if (!(entry.getValue() instanceof Map)) {
                if (entry.getValue() instanceof String) {
                    DtoLogDetail logDetail = DtoLogDetail.builder()
                            .colname(entry.getKey())
                            .newValue(entry.getValue().toString().replace("'", "").replace("'", ""))
                            .build();
                    logDetails.add(logDetail);
                } else {
                    DtoLogDetail logDetail = DtoLogDetail.builder()
                            .colname(entry.getKey())
                            .newValue("" + entry.getValue())
                            .build();
                    logDetails.add(logDetail);
                }
            }
        }

        DtoLogDetail logDetail = DtoLogDetail.builder()
                .colname("create_at")
                .newValue(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS")))
                .build();
        logDetails.add(logDetail);

        DtoLog dtoLog = DtoLog.builder()
                .operation("DELETE")
                .message("Hapus Data " + tableName)
                .tbname(tableName)
                .tbkey(pk)
                .username(headersService.getUsername())
                .ipClient(headersService.getIpClient())
                .createAt(LocalDateTime.now())
                .logsDetails(logDetails)
                .build();

        streamBridge.send("logData-out-0", dtoLog);
    }
}
