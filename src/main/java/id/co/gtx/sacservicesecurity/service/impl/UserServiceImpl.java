package id.co.gtx.sacservicesecurity.service.impl;

import id.co.gtx.sacmodules.dto.*;
import id.co.gtx.sacmodules.enumz.Status;
import id.co.gtx.sacmodules.exception.OperationException;
import id.co.gtx.sacmodules.service.HeadersService;
import id.co.gtx.sacmodules.service.ServiceErrorHandling;
import id.co.gtx.sacmodules.utils.MapperUtil;
import id.co.gtx.sacservicesecurity.model.dto.*;
import id.co.gtx.sacservicesecurity.model.dto.DtoGroup;
import id.co.gtx.sacservicesecurity.model.entity.postgresql.Group;
import id.co.gtx.sacservicesecurity.model.entity.postgresql.ResourceAccess;
import id.co.gtx.sacservicesecurity.model.entity.postgresql.Role;
import id.co.gtx.sacservicesecurity.model.entity.postgresql.User;
import id.co.gtx.sacservicesecurity.repository.postgresql.GroupRepository;
import id.co.gtx.sacservicesecurity.repository.postgresql.ResourceAccessRepository;
import id.co.gtx.sacservicesecurity.repository.postgresql.RoleRepository;
import id.co.gtx.sacservicesecurity.repository.postgresql.UserRepository;
import id.co.gtx.sacservicesecurity.service.UserService;
import org.springframework.dao.DataAccessException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClient;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClientRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

@Service
public class UserServiceImpl implements UserService {

    private final HeadersService headersService;

    private final MapperUtil mapperUtil;

    private final PasswordEncoder passwordEncoder;

    private final RegisteredClientRepository registeredClientRepository;

    private final ResourceAccessRepository resourceAccessRepository;

    private final RoleRepository roleRepository;

    private final ServiceErrorHandling serviceErrorHandling;

    private final GroupRepository groupRepository;

    private final UserRepository userRepository;

    public UserServiceImpl(HeadersService headersService, MapperUtil mapperUtil, PasswordEncoder passwordEncoder,
                           RegisteredClientRepository registeredClientRepository, ResourceAccessRepository resourceAccessRepository,
                           RoleRepository roleRepository, ServiceErrorHandling serviceErrorHandling, GroupRepository groupRepository,
                           UserRepository userRepository) {
        this.headersService = headersService;
        this.mapperUtil = mapperUtil;
        this.passwordEncoder = passwordEncoder;
        this.registeredClientRepository = registeredClientRepository;
        this.resourceAccessRepository = resourceAccessRepository;
        this.roleRepository = roleRepository;
        this.serviceErrorHandling = serviceErrorHandling;
        this.groupRepository = groupRepository;
        this.userRepository = userRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public DtoResponse<List<DtoUserGet>> findAll() {
        List<User> users = userRepository.findAll();
        if (users.isEmpty()) {
            throw new OperationException(DtoResponse.builder()
                    .code(StatusCode.FAILED)
                    .status(Status.FAILED)
                    .message("Data User Kosong")
                    .build());
        }
        List<DtoUserGet> userGets = getGroup(users);
        return new DtoResponse<>(StatusCode.SUCCESS, Status.SUCCESS, "Berhasil Menampilkan Semua Data User", userGets);
    }

    @Override
    @Transactional(readOnly = true)
    public DtoResponse<DtoUserGet> findById(String username) {
        Optional<User> optionalUser = userRepository.findById(username);
        User user = optionalUser.orElseThrow(() -> new OperationException(DtoResponse.builder()
                .code(StatusCode.FAILED)
                .status(Status.FAILED)
                .message("Username " + username + " tidak ditemukan")
                .build()));
        return new DtoResponse<>(StatusCode.SUCCESS, Status.SUCCESS, "Data User Ditemukan", getGroup(user));
    }

    @Override
    @Transactional(readOnly = true)
    public DtoResponse<DtoUserGetName> findNameById(String username) {
        if (!"".equals(username) && username != null) {
            DtoUserGetName name = userRepository.findNameByUsername(username).orElseThrow(() -> new OperationException(DtoResponse.builder()
                    .code(StatusCode.FAILED)
                    .status(Status.FAILED)
                    .message("Username " + username + " tidak ditemukan")
                    .build()));
            return new DtoResponse<>(StatusCode.SUCCESS, Status.SUCCESS, "Nama Lengkap User Ditemukan", name);
        }
        throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "Username harus diisi"));
    }

    @Override
    @Transactional(rollbackFor = {DataAccessException.class})
    public DtoResponse<DtoUserGet> createUser(DtoUser dtoUser) {
        serviceErrorHandling.validateSchema(dtoUser).ifPresent(error -> {
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED_CREATE, Status.FAILED, "Gagal menambakan data User baru", null, error));
        });

        if (dtoUser.isDefaultUser() && (!headersService.isDefaultUser() || !headersService.getAuthorities().contains("ROLE_SUPER_ADMIN_WEB"))) {
            DtoError error = new DtoError("invalid_authority", "User login bukan SUPER_ADMIN dan bukan Default User");
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED_CREATE, Status.FAILED, "Gagal menambakan data User baru", null, error));
        }

        userRepository.findById(dtoUser.getUsername())
                .ifPresent(user -> {
                    DtoError.DtoErrorBuilder error = DtoError.builder()
                            .code(ErrorCode.ID_TAKEN)
                            .desc("Data inputan sudah ada");

                    List<DtoErrorDetail> errorDetails = new ArrayList<>();
                    errorDetails.add(new DtoErrorDetail("Field", "username", "Username sudah terdaftar", dtoUser.getClass().getName().replace("id.co.gtx.sacservicesecurity.", ""), "Taken", user.getUsername()));

                    if (user.getEmail().equals(dtoUser.getEmail())) {
                        errorDetails.add(new DtoErrorDetail("Field", "email", "Email sudah terdaftar", dtoUser.getClass().getName().replace("id.co.gtx.sacservicesecurity.", ""), "Taken", user.getEmail()));
                    }

                    error.errorDetails(errorDetails);

                    throw new OperationException(DtoResponse.builder()
                            .code(StatusCode.FAILED_CREATE)
                            .status(Status.FAILED)
                            .message("Gagal menambakan data User baru")
                            .error(error.build())
                            .build());
                });

        userRepository.findByEmail(dtoUser.getEmail())
                .ifPresent(user -> {
                    DtoError.DtoErrorBuilder error = DtoError.builder()
                            .code(ErrorCode.ID_TAKEN)
                            .desc("Data inputan sudah ada");

                    List<DtoErrorDetail> errorDetails = new ArrayList<>();
                    errorDetails.add(new DtoErrorDetail("Field", "email", "Email sudah terdaftar", dtoUser.getClass().getName().replace("id.co.gtx.sacservicesecurity.", ""), "Taken", user.getEmail()));

                    if (user.getEmail().equals(dtoUser.getEmail())) {
                        errorDetails.add(new DtoErrorDetail("Field", "username", "Username sudah terdaftar", dtoUser.getClass().getName().replace("id.co.gtx.sacservicesecurity.", ""), "Taken", user.getUsername()));
                    }

                    error.errorDetails(errorDetails);

                    throw new OperationException(DtoResponse.builder()
                            .code(StatusCode.FAILED_CREATE)
                            .status(Status.FAILED)
                            .message("Gagal menambakan data User baru")
                            .error(error.build())
                            .build());
                });

        if (!dtoUser.getPassword().equals(dtoUser.getConfirmPassword())) {
            DtoError error = new DtoError(ErrorCode.INVALID_PAYLOAD, "Data inputan tidak valid", new ArrayList<>());
            error.addError("Field", "confirm_password", "Konfirmasi Password Tidak Sesuai", dtoUser.getClass().getName().replace("id.co.gtx.sacservicesecurity.", ""), "Taken", dtoUser.getConfirmPassword());
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED_CREATE, Status.FAILED, "Gagal menambakan data User baru", null, error));
        }

        if (dtoUser.getExpiredDate().isBefore(LocalDate.now())) {
            DtoError error = new DtoError(ErrorCode.INVALID_PAYLOAD, "Data inputan tidak valid", new ArrayList<>());
            error.addError("Field", "expired_date", "Tanggal Expired tidak boleh kurang dari tanggal sekarang", dtoUser.getClass().getName().replace("id.co.gtx.sacservicesecurity.", ""), "Taken", dtoUser.getExpiredDate());
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED_CREATE, Status.FAILED, "Gagal menambakan data User baru", null, error));
        }

        if (dtoUser.getExpiredPasswordDate().isBefore(LocalDate.now())) {
            DtoError error = new DtoError(ErrorCode.INVALID_PAYLOAD, "Data inputan tidak valid", new ArrayList<>());
            error.addError("Field", "expired_password_date", "Tanggal Expired Password tidak boleh kurang dari tanggal sekarang", dtoUser.getClass().getName().replace("id.co.gtx.sacservicesecurity.", ""), "Taken", dtoUser.getExpiredPasswordDate());
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED_CREATE, Status.FAILED, "Gagal menambakan data User baru", null, error));
        }

        User user = mapperUtil.mappingClass(dtoUser, User.class);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setCreateAt(LocalDateTime.now());

        if (user.getResourceAccesses() != null) {
            user.getResourceAccesses().forEach(access -> {
                access.getRoles().forEach(role ->
                        roleRepository.findById(role.getId())
                                .orElseThrow(() ->
                                        new OperationException(
                                                new DtoResponse<>(StatusCode.FAILED_NOT_FOUND, Status.FAILED, "Role ID " + role.getId() + " tidak ditemukan"))
                                ));
                RegisteredClient client = registeredClientRepository.findByClientId(access.getClientId());
                if (client == null)
                    throw new OperationException(new DtoResponse<>(StatusCode.FAILED_NOT_FOUND, Status.FAILED, "Client ID " + access.getClientId() + " tidak ditemukan"));

                access.setUsername(user.getUsername());
            });
        }

        Set<Group> groups = new HashSet<>();
        if (dtoUser.getGroups() != null) {
            dtoUser.getGroups().forEach(group -> {
                Group userGroup = new Group(dtoUser.getUsername(), group.getGroupId(), group.getGroupParent(), group.getType().shortValue(), user);
                groups.add(userGroup);
            });
            user.setGroups(groups);
        }

        try {
            DtoUserGet userSaved = mapperUtil.mappingClass(userRepository.save(user), DtoUserGet.class);
            userSaved.setGroups(dtoUser.getGroups());
            return new DtoResponse<>(StatusCode.SUCCESS_CREATE, Status.SUCCESS, "Berhasil menambahkan data User baru", userSaved);
        } catch (DataAccessException e) {
            throw serviceErrorHandling.handleDataAccessException(e, StatusCode.FAILED_CREATE, Status.FAILED, "Gagal menambahkan data User baru");
        }
    }

    @Override
    @Transactional(rollbackFor = {DataAccessException.class})
    public DtoResponse<DtoUserGet> updateUser(DtoUserUpdate dtoUser) {
        serviceErrorHandling.validateSchema(dtoUser).ifPresent(error -> {
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED_CREATE, Status.FAILED, "Gagal update data User", null, error));
        });

        if (dtoUser.isDefaultUser() && (!headersService.isDefaultUser() || !headersService.getAuthorities().contains("ROLE_SUPER_ADMIN_WEB"))) {
            DtoError error = new DtoError("invalid_authority", "User login bukan SUPER_ADMIN dan bukan Default User");
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED_UPDATE, Status.FAILED, "Gagal Update Data User", null, error));
        }

        User user = userRepository.findByEmailAndUsername(dtoUser.getEmail(), dtoUser.getUsername())
                        .orElseGet(() ->
                                userRepository.findById(dtoUser.getUsername()).orElseThrow(() -> {
                                    DtoError error = new DtoError(ErrorCode.INVALID_PAYLOAD, "Data inputan tidak ditemukan", new ArrayList<>());
                                    error.addError(new DtoErrorDetail("Field", "username", "Username tidak ditemukan", dtoUser.getClass().getName().replace("id.co.gtx.sacservicesecurity.", ""), "Taken", dtoUser.getUsername()));
                                    return new OperationException(
                                            new DtoResponse<>(StatusCode.FAILED_NOT_FOUND, Status.FAILED, "Gagal update data User"));
                                }));

        userRepository.findByEmailAndUsernameNot(dtoUser.getEmail(), dtoUser.getUsername())
                .ifPresent(userExist -> {
                    DtoError.DtoErrorBuilder error = DtoError.builder()
                            .code(ErrorCode.ID_TAKEN)
                            .desc("Data inputan sudah ada");

                    List<DtoErrorDetail> errorDetails = new ArrayList<>();
                    errorDetails.add(new DtoErrorDetail("Field", "email", "Email sudah terdaftar", dtoUser.getClass().getName().replace("id.co.gtx.sacservicesecurity.", ""), "Taken", dtoUser.getEmail()));

                    error.errorDetails(errorDetails);

                    throw new OperationException(DtoResponse.builder()
                            .code(StatusCode.FAILED_CREATE)
                            .status(Status.FAILED)
                            .message("Gagal update data User")
                            .error(error.build())
                            .build());
                });

        if (dtoUser.getExpiredDate().isBefore(LocalDate.now())) {
            DtoError error = new DtoError(ErrorCode.INVALID_PAYLOAD, "Data inputan tidak valid", new ArrayList<>());
            error.addError("Field", "expired_date", "Tanggal Expired tidak boleh kurang dari tanggal sekarang", dtoUser.getClass().getName().replace("id.co.gtx.sacservicesecurity.", ""), "Taken", dtoUser.getExpiredDate());
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED_CREATE, Status.FAILED, "Gagal update data User", null, error));
        }

        if (dtoUser.getExpiredPasswordDate().isBefore(LocalDate.now())) {
            DtoError error = new DtoError(ErrorCode.INVALID_PAYLOAD, "Data inputan tidak valid", new ArrayList<>());
            error.addError("Field", "expired_password_date", "Tanggal Expired Password tidak boleh kurang dari tanggal sekarang", dtoUser.getClass().getName().replace("id.co.gtx.sacservicesecurity.", ""), "Taken", dtoUser.getExpiredPasswordDate());
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED_CREATE, Status.FAILED, "Gagal update data User", null, error));
        }

        User userUpdate = mapperUtil.mappingClass(dtoUser, User.class);
        userUpdate.setPassword(user.getPassword());
        userUpdate.setResourceAccesses(user.getResourceAccesses());
        userUpdate.setGroups(user.getGroups());
        userUpdate.setCreateAt(user.getCreateAt());
        userUpdate.setUpdateAt(LocalDateTime.now());

        Set<ResourceAccess> resourceAccesses = new HashSet<>();
        if (dtoUser.getResourceAccesses() != null) {
            dtoUser.getResourceAccesses().forEach(access -> {
                access.getRoles().forEach(role ->
                        roleRepository.findById(role.getId())
                                .orElseThrow(() ->
                                        new OperationException(
                                                new DtoResponse<>(StatusCode.FAILED_NOT_FOUND, Status.FAILED, "Role ID " + role.getId() + " tidak ditemukan"))
                                ));

                RegisteredClient client = registeredClientRepository.findByClientId(access.getClientId());
                if (client == null)
                    throw new OperationException(new DtoResponse<>(StatusCode.FAILED_NOT_FOUND, Status.FAILED, "Client ID " + access.getClientId() + " tidak ditemukan"));

                ResourceAccess resourceAccess = new ResourceAccess(dtoUser.getUsername(), access.getClientId(),
                        userUpdate, mapperUtil.mappingClass(access.getRoles(), Role.class));
                resourceAccesses.add(resourceAccess);
            });
            userUpdate.setResourceAccesses(resourceAccesses);
        }

        Set<Group> groups = new HashSet<>();
        if (dtoUser.getGroups() != null) {
            dtoUser.getGroups().forEach(group -> {
                Group userGroup = new Group(dtoUser.getUsername(), group.getGroupId(), group.getGroupParent(), group.getType().shortValue(), userUpdate);
                groups.add(userGroup);
            });
            userUpdate.setGroups(groups);
        } else {
            dtoUser.setGroups(new HashSet<>());
        }

        try {
            DtoUserGet userSaved = mapperUtil.mappingClass(userRepository.save(userUpdate), DtoUserGet.class);
            if (resourceAccesses.size() == 0) {
                resourceAccessRepository.deleteByUsername(userSaved.getUsername());
                userSaved.setResourceAccesses(new HashSet<>());
            }
            userSaved.setGroups(dtoUser.getGroups());
            groupRepository.deleteByUsername(userSaved.getUsername());
            if (groups.size() > 0) {
                groupRepository.saveAllAndFlush(groups);
            }
            return new DtoResponse<>(StatusCode.SUCCESS_UPDATE, Status.SUCCESS, "Berhasil update data User ", userSaved);
        } catch (DataAccessException e) {
            throw serviceErrorHandling.handleDataAccessException(e, StatusCode.FAILED_UPDATE, Status.FAILED, "Gagal update data User");
        }
    }

    @Override
    @Transactional(rollbackFor = {DataAccessException.class})
    public DtoResponse<DtoUserGet> deleteUser(String username) {
        try {
            User user = userRepository.findById(username).orElseThrow(() -> {
                DtoError error = new DtoError(ErrorCode.INVALID_PAYLOAD, "Data inputan tidak ditemukan", new ArrayList<>());
                error.addError(new DtoErrorDetail("Field", "username", "Username tidak ditemukan", null, "Taken", username));
                return new OperationException(new DtoResponse<>(StatusCode.FAILED_NOT_FOUND, Status.FAILED, "Gagal hapus data User", null, error));
            });

            if (user.isDefaultUser() && (!headersService.isDefaultUser() || !headersService.getAuthorities().contains("ROLE_SUPER_ADMIN_WEB"))) {
                DtoError error = new DtoError("invalid_authority", "User login bukan SUPER_ADMIN dan bukan Default User");
                throw new OperationException(new DtoResponse<>(StatusCode.FAILED_UPDATE, Status.FAILED, "Gagal Update Data User", null, error));
            }

            userRepository.delete(user);
            return new DtoResponse<>(StatusCode.SUCCESS_DELETE, Status.SUCCESS, "Berhasil hapus data User", getGroup(user));
        } catch (DataAccessException e) {
            throw serviceErrorHandling.handleDataAccessException(e, StatusCode.FAILED_DELETE, Status.FAILED, "Gagal hapus data User");
        }
    }

    @Override
    @Transactional(rollbackFor = {DataAccessException.class})
    public DtoResponse<DtoUserGet> passwordChange(String username, DtoPasswordChange passwordChange) {
        serviceErrorHandling.validateSchema(passwordChange).ifPresent(error -> {
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED_CREATE, Status.FAILED, "Gagal mengganti password User", null, error));
        });

        if (passwordChange.getNewPassword().equals(passwordChange.getOldPassword())) {
            DtoError error = new DtoError(ErrorCode.INVALID_PAYLOAD, "Data inputan tidak valid", new ArrayList<>());
            error.addError(new DtoErrorDetail("Field", "new_password", "Password Baru tidak boleh sama dengan Password Lama", passwordChange.getClass().getName().replace("id.co.gtx.sacservicesecurity.", ""), "Taken", passwordChange.getNewPassword()));
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED_UPDATE, Status.FAILED, "Gagal mengganti password User", null, error));
        }

        if (!passwordChange.getNewPassword().equals(passwordChange.getConfirmPassword())) {
            DtoError error = new DtoError(ErrorCode.INVALID_PAYLOAD, "Data inputan tidak valid", new ArrayList<>());
            error.addError(new DtoErrorDetail("Field", "confirm_password", "Password Konfirmasi tidak sesuai", passwordChange.getClass().getName().replace("id.co.gtx.sacservicesecurity.", ""), "Taken", passwordChange.getConfirmPassword()));
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED_UPDATE, Status.FAILED, "Gagal mengganti password User", null, error));
        }

        User user = userRepository.findById(username).orElseThrow(() -> {
            DtoError error = new DtoError(ErrorCode.INVALID_PAYLOAD, "Data inputan tidak ditemukan", new ArrayList<>());
            error.addError(new DtoErrorDetail("Field", "username", "Username tidak ditemukan", null, "Taken", username));
            return new OperationException(new DtoResponse<>(StatusCode.FAILED_NOT_FOUND, Status.FAILED, "Gagal mengganti password User", null, error));
        });

        if (user.isDefaultUser() && (!headersService.isDefaultUser() || !headersService.getAuthorities().contains("ROLE_SUPER_ADMIN_WEB"))) {
            DtoError error = new DtoError("invalid_authority", "User login bukan SUPER_ADMIN dan bukan Default User");
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED_UPDATE, Status.FAILED, "Gagal Update Data User", null, error));
        }

        if (!passwordEncoder.matches(passwordChange.getOldPassword(), user.getPassword())) {
            DtoError error = new DtoError(ErrorCode.INVALID_PAYLOAD, "Data inputan tidak ditemukan", new ArrayList<>());
            error.addError(new DtoErrorDetail("Field", "old_password", "Password Lama tidak sesuai", passwordChange.getClass().getName().replace("id.co.gtx.sacservicesecurity.", ""), "Taken", passwordChange.getOldPassword()));
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED_NOT_FOUND, Status.FAILED, "Gagal mengganti password User", null, error));
        }

        user.setPassword(passwordEncoder.encode(passwordChange.getNewPassword()));

        try {
            userRepository.updateUserPassword(user);
            return new DtoResponse<>(StatusCode.SUCCESS_UPDATE, Status.SUCCESS, "Berhasil mengganti password User " + username, getGroup(user));
        } catch (DataAccessException e) {
            throw serviceErrorHandling.handleDataAccessException(e, StatusCode.FAILED_UPDATE, Status.FAILED, "Gagal mengganti password User");
        }
    }

    @Override
    @Transactional(rollbackFor = {DataAccessException.class})
    public DtoResponse<DtoUserGet> passwordReset(String username, DtoPasswordReset passwordReset) {
        serviceErrorHandling.validateSchema(passwordReset).ifPresent(error -> {
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED_CREATE, Status.FAILED, "Gagal reset password User", null, error));
        });

        if (!passwordReset.getNewPassword().equals(passwordReset.getConfirmPassword())) {
            DtoError error = new DtoError(ErrorCode.INVALID_PAYLOAD, "Data inputan tidak valid", new ArrayList<>());
            error.addError(new DtoErrorDetail("Field", "confirm_password", "Password Konfirmasi tidak sesuai", passwordReset.getClass().getName().replace("id.co.gtx.sacservicesecurity.", ""), "Taken", passwordReset.getConfirmPassword()));
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED_UPDATE, Status.FAILED, "Gagal reset password User", null, error));
        }

        User user = userRepository.findById(username).orElseThrow(() -> {
            DtoError error = new DtoError(ErrorCode.INVALID_PAYLOAD, "Data inputan tidak ditemukan", new ArrayList<>());
            error.addError(new DtoErrorDetail("Field", "username", "Username tidak ditemukan", null, "Taken", username));
            return new OperationException(new DtoResponse<>(StatusCode.FAILED_NOT_FOUND, Status.FAILED, "Gagal reset password User", null, error));
        });

        if (passwordEncoder.matches(passwordReset.getNewPassword(), user.getPassword())) {
            DtoError error = new DtoError(ErrorCode.INVALID_PAYLOAD, "Data inputan tidak valid", new ArrayList<>());
            error.addError(new DtoErrorDetail("Field", "new_password", "Password Baru tidak boleh sama dengan Password Lama", passwordReset.getClass().getName().replace("id.co.gtx.sacservicesecurity.", ""), "Taken", passwordReset.getNewPassword()));
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED_UPDATE, Status.FAILED, "Gagal reset password User", null, error));
        }

        user.setPassword(passwordEncoder.encode(passwordReset.getNewPassword()));
        try {
            userRepository.updateUserPassword(user);
            return new DtoResponse<>(StatusCode.SUCCESS_UPDATE, Status.SUCCESS, "Berhasil reset password User", getGroup(user));
        } catch (DataAccessException e) {
            throw serviceErrorHandling.handleDataAccessException(e, StatusCode.FAILED_UPDATE, Status.FAILED, "Gagal reset password User");
        }
    }

    private DtoUserGet getGroup(User user) {
        Set<DtoGroup> groups = new HashSet<>();
        user.getGroups().forEach(group -> groups.add(mapperUtil.mappingClass(group, DtoGroup.class)));
        DtoUserGet userGet = mapperUtil.mappingClass(user, DtoUserGet.class);
        userGet.setGroups(groups);
        return userGet;
    }

    private List<DtoUserGet> getGroup(List<User> users) {
        List<DtoUserGet> userGets = new ArrayList<>();
        users.forEach(user -> {
            Set<DtoGroup> groups = new HashSet<>();
            user.getGroups().forEach(group -> groups.add(mapperUtil.mappingClass(group, DtoGroup.class)));
            DtoUserGet userGet = mapperUtil.mappingClass(user, DtoUserGet.class);
            userGet.setGroups(groups);
            userGets.add(userGet);
        });
        return userGets;
    }
}
