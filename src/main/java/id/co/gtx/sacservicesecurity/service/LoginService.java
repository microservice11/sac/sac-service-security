package id.co.gtx.sacservicesecurity.service;

import id.co.gtx.sacmodules.dto.DtoResponse;
import id.co.gtx.sacservicesecurity.model.dto.DtoLogin;
import id.co.gtx.sacservicesecurity.model.dto.DtoOtp;
import id.co.gtx.sacservicesecurity.model.dto.DtoUserActive;
import id.co.gtx.sacservicesecurity.model.dto.DtoUserGet;

import java.util.List;
import java.util.Map;

public interface LoginService {
    DtoResponse<DtoUserGet> doCheck(DtoLogin dtoLogin);

    DtoResponse<?> doCheck(DtoLogin dtoLogin, String otpType);

    DtoResponse<Map<String, Object>> doLogin(DtoOtp otp);

    DtoResponse<?> doLogout();

    DtoResponse<Map<String, Object>> doRefresh(Map<String, String> refreshToken);

    DtoResponse<List<DtoUserActive>> userActives();

    DtoResponse<?> doTerminate(String id);
}
