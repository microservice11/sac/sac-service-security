package id.co.gtx.sacservicesecurity.service.impl;

import id.co.gtx.sacmodules.dto.*;
import id.co.gtx.sacmodules.enumz.OtpType;
import id.co.gtx.sacmodules.enumz.Status;
import id.co.gtx.sacmodules.exception.OperationException;
import id.co.gtx.sacmodules.service.HeadersService;
import id.co.gtx.sacmodules.service.ServiceErrorHandling;
import id.co.gtx.sacmodules.utils.MapperUtil;
import id.co.gtx.sacservicesecurity.model.dto.*;
import id.co.gtx.sacservicesecurity.model.entity.postgresql.User;
import id.co.gtx.sacservicesecurity.model.entity.redis.Otp;
import id.co.gtx.sacservicesecurity.repository.postgresql.UserRepository;
import id.co.gtx.sacservicesecurity.repository.redis.OtpRepository;
import id.co.gtx.sacservicesecurity.service.LoginService;
import id.co.gtx.sacservicesecurity.service.MenuService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.env.Environment;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.core.OAuth2TokenType;
import org.springframework.security.oauth2.server.authorization.JdbcOAuth2AuthorizationService;
import org.springframework.security.oauth2.server.authorization.OAuth2Authorization;
import org.springframework.security.oauth2.server.authorization.OAuth2AuthorizationService;
import org.springframework.security.oauth2.server.resource.web.DefaultBearerTokenResolver;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.StandardCharsets;
import java.security.Principal;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.util.*;

@Slf4j
@Service
public class LoginServiceImpl implements LoginService {

    private static final RestTemplate restTemplate;

    static {
        restTemplate = new RestTemplate();
    }

    private String host;
    private String hostLogout;
    private String clientId;
    private String clientSecret;

    private final DefaultBearerTokenResolver tokenResolver;
    private final Environment env;
    private final HeadersService headers;
    private final MapperUtil mapperUtil;
    private final MenuService menuService;
    private final OAuth2AuthorizationService authorizationService;
    private final OtpRepository otpRepository;
    private final PasswordEncoder passwordEncoder;
    private final ServiceErrorHandling serviceErrorHandling;
    private final StreamBridge streamBridge;
    private final UserDetailsServiceImpl userDetailsService;
    private final UserRepository userRepository;

    public LoginServiceImpl(DefaultBearerTokenResolver tokenResolver,
                            Environment env, HeadersService headers,
                            MapperUtil mapperUtil,
                            MenuService menuService,
                            OAuth2AuthorizationService authorizationService,
                            OtpRepository otpRepository,
                            PasswordEncoder passwordEncoder,
                            ServiceErrorHandling serviceErrorHandling,
                            StreamBridge streamBridge,
                            UserDetailsServiceImpl userDetailsService,
                            UserRepository userRepository) {
        this.tokenResolver = tokenResolver;
        this.env = env;
        this.headers = headers;
        this.mapperUtil = mapperUtil;
        this.menuService = menuService;
        this.authorizationService = authorizationService;
        this.otpRepository = otpRepository;
        this.passwordEncoder = passwordEncoder;
        this.serviceErrorHandling = serviceErrorHandling;
        this.streamBridge = streamBridge;
        this.userDetailsService = userDetailsService;
        this.userRepository = userRepository;
    }

    @Override
    @Transactional(rollbackFor = {DataAccessException.class}, noRollbackFor = {OperationException.class})
    public DtoResponse<DtoUserGet> doCheck(DtoLogin dtoLogin) {
        serviceErrorHandling.validateSchema(dtoLogin).ifPresent(error -> {
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "Gagal Login", null, error));
        });

        UserDetails userDetails = userDetailsService.loadUserByUsername(dtoLogin.getUsername());
        User user = userDetailsService.getUser();

        boolean match = passwordEncoder.matches(dtoLogin.getPassword(), userDetails.getPassword());
        if (!match) {
            user.setFailPass((short) (user.getFailPass() + 1));
        } else {
            user.setFailPass((short) 0);
        }

        try {
            ((JdbcOAuth2AuthorizationService) authorizationService).removeNotActive();
            userRepository.updateUserFailPass(user);
            if (!match) {
                DtoError error = new DtoError(ErrorCode.INVALID_PAYLOAD, "Data inputan tidak valid", new ArrayList<>());
                error.addError(new DtoErrorDetail("Field", "password", "Password tidak sesuai", "InvalidPayload"));
                throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "Password tidak sesuai", null, error));
            }
        } catch (DataAccessException e) {
            throw serviceErrorHandling.handleDataAccessException(e, StatusCode.FAILED, Status.FAILED, "Gagal update kolom fail_pass");
        }

        return new DtoResponse<>(StatusCode.SUCCESS, Status.SUCCESS, "Username dan Password valid", mapperUtil.mappingClass(user, DtoUserGet.class));
    }

    @Override
    @Transactional
    public DtoResponse<?> doCheck(DtoLogin dtoLogin, String otpType) {
        DtoUserGet dtoUser = doCheck(dtoLogin).getData();
        List<Otp> otps = otpRepository.findFirstByUsername(dtoUser.getUsername());
        if (otps.size() > 0) {
            otpRepository.deleteAll(otps);
        }

        String ipClient = headers.getRequest().getHeader("X-FORWARDED-FOR") != null ? headers.getRequest().getHeader("X-FORWARDED-FOR") : headers.getRequest().getRemoteAddr();
        Otp otp = new Otp();
        otp.setEmail(dtoUser.getEmail());
        otp.setUsername(dtoUser.getUsername());
        otp.setTelegramId(dtoUser.getTelegramId());
        otp.setOtp(generateOTP());
        otp.setIpClient(ipClient);
        otp.setOtpType(Enum.valueOf(OtpType.class, otpType.toUpperCase()));
        otpRepository.save(otp);
        streamBridge.send("otpData-out-0", otp);

        return new DtoResponse<>(StatusCode.SUCCESS, Status.SUCCESS, "Otp Sudah Dikirim Melalui " + otpType.toUpperCase());
    }

    @Override
    @Transactional
    public DtoResponse<Map<String, Object>> doLogin(DtoOtp dtoOtp) {
        validateAuthBasic();
        List<Otp> otps = otpRepository.findFirstByUsernameAndOtp(dtoOtp.getUsername(), dtoOtp.getOtp());
        if (otps.isEmpty()) {
            DtoError error = new DtoError(ErrorCode.INVALID_PAYLOAD, "Data inputan tidak valid", new ArrayList<>());
            error.addError(new DtoErrorDetail("Field", "otp", "OTP Tidak Sesuai atau Sudah Kadaluarsa", "InvalidPayload"));
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "Gagal Login", null, error));
        }
        otpRepository.deleteAll(otps);

        try {
            String ipAddress = headers.getRequest().getHeader("X-FORWARDED-FOR") != null ? headers.getRequest().getHeader("X-FORWARDED-FOR") : headers.getRequest().getRemoteAddr();
            MultiValueMap<String, String> formData = new LinkedMultiValueMap<>();
            formData.add("username", dtoOtp.getUsername());
            formData.add("password", dtoOtp.getPassword());
            formData.add("grant_type", "password");

            HttpHeaders httpHeaders = new HttpHeaders();
            String encodeBasicAuth = Base64.getEncoder().encodeToString((clientId + ":" + clientSecret).getBytes(StandardCharsets.UTF_8));
            httpHeaders.set("Authorization", "Basic " + encodeBasicAuth);
            httpHeaders.set("Content-Type", "application/x-www-form-urlencoded");
            httpHeaders.set("X-FORWARDED-FOR", ipAddress);

            Map<String, Object> tokenDetail = restTemplate.exchange(host, HttpMethod.POST, new HttpEntity(formData, httpHeaders),
                    new ParameterizedTypeReference<HashMap<String, Object>>(){}, new Object[0]).getBody();
            headers.setTokenValue((String) tokenDetail.get("access_token"));
            tokenDetail.putAll(mapperUtil.mappingClass(menuService.findMenuWeb().getData()));
            return new DtoResponse<>(StatusCode.SUCCESS, Status.SUCCESS, "Berhasil Login", tokenDetail);
        } catch (RestClientResponseException ex) {
            ex.printStackTrace();
            DtoError errObj = new DtoError(ErrorCode.REST_CLIENT_RESPONSE_EXCEPTION, "permasalahan akses data api", new ArrayList());
            if (!"".equals(ex.getResponseBodyAsString())) {
                Map map = mapperUtil.mappingClass(ex.getResponseBodyAsString(), Map.class);
                if (map != null) {
                    if (map.get("error") != null) {
                        errObj = new DtoError((String) map.get("error"), "login invalid", null);
                        throw new OperationException(new DtoResponse<>(StatusCode.FAILED,
                                Status.FAILED,
                                map.get("error_description") != null ? (String) map.get("error_description") : "Gagal Login",
                                null,
                                errObj));
                    } else {
                        for (Object key : map.keySet()) {
                            errObj.addError(new DtoErrorDetail("metadata", key + "", (String) map.get(key), null));
                        }
                    }
                }
            }
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "Gagal Login", null, errObj));
        }
    }

    @Override
    public DtoResponse<?> doLogout() {
        String token = tokenResolver.resolve(headers.getRequest());
        if (token == null)
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "Access Token is null"));

        validateAuthBasic();

        OAuth2Authorization authorization = authorizationService.findByToken(token, OAuth2TokenType.ACCESS_TOKEN);
        if (authorization == null)
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED_NOT_FOUND, Status.FAILED, "Access Token not found"));

        try {
            authorizationService.remove(authorization);
            return new DtoResponse<>(StatusCode.SUCCESS, Status.SUCCESS, "Berhasil Logout");
        } catch (DataAccessException e) {
            throw serviceErrorHandling.handleDataAccessException(e, StatusCode.FAILED, Status.FAILED, "Gagal Logout");
        }
    }

    @Override
    public DtoResponse<Map<String, Object>> doRefresh(Map<String, String> mapBody) {
        if (mapBody == null) {
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "refresh_token cannot null"));
        }

        String refreshToken = mapBody.get("refresh_token");
        if ("".equals(refreshToken) || refreshToken == null) {
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "refresh_token cannot empty"));
        }

        validateAuthBasic();

        try {
            String ipAddress = headers.getRequest().getHeader("X-FORWARDED-FOR") != null ? headers.getRequest().getHeader("X-FORWARDED-FOR") : headers.getRequest().getRemoteAddr();
            MultiValueMap<String, String> formData = new LinkedMultiValueMap<>();
            formData.add("refresh_token", refreshToken);
            formData.add("grant_type", "refresh_token");

            HttpHeaders httpHeaders = new HttpHeaders();
            String encodeBasicAuth = Base64.getEncoder().encodeToString((clientId + ":" + clientSecret).getBytes(StandardCharsets.UTF_8));
            httpHeaders.set("Authorization", "Basic " + encodeBasicAuth);
            httpHeaders.set("Content-Type", "application/x-www-form-urlencoded");
            httpHeaders.set("X-FORWARDED-FOR", ipAddress);

            Map<String, Object> tokenDetail = restTemplate.exchange(host, HttpMethod.POST, new HttpEntity(formData, httpHeaders),
                    new ParameterizedTypeReference<HashMap<String, Object>>(){}, new Object[0]).getBody();
            headers.setTokenValue((String) tokenDetail.get("access_token"));
            tokenDetail.putAll(mapperUtil.mappingClass(menuService.findMenuWeb().getData()));
            return new DtoResponse<>(StatusCode.SUCCESS, Status.SUCCESS, "Berhasil Refresh Token", tokenDetail);
        } catch (RestClientResponseException ex) {
            ex.printStackTrace();
            DtoError errObj = new DtoError(ErrorCode.REST_CLIENT_RESPONSE_EXCEPTION, "permasalahan akses data api", new ArrayList());
            if (!"".equals(ex.getResponseBodyAsString())) {
                Map map = mapperUtil.mappingClass(ex.getResponseBodyAsString(), Map.class);
                if (map != null) {
                    if (map.get("error") != null) {
                        errObj = new DtoError((String) map.get("error"), "refresh_token invalid", null);
                        throw new OperationException(new DtoResponse<>(StatusCode.FAILED,
                                Status.FAILED,
                                map.get("error_description") != null ? (String) map.get("error_description") : "Gagal Refresh Token",
                                null,
                                errObj));
                    } else {
                        for (Object key : map.keySet()) {
                            errObj.addError(new DtoErrorDetail("metadata", key + "", (String) map.get(key), null));
                        }
                    }
                }
            }
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "Gagal Refresh Token", null, errObj));
        }
    }

    @Override
    @Transactional(readOnly = true)
    public DtoResponse<List<DtoUserActive>> userActives() {
        JdbcOAuth2AuthorizationService authorizationService = (JdbcOAuth2AuthorizationService) this.authorizationService;
        List<OAuth2Authorization> authorizations = authorizationService.findAll();
        List<DtoUserActive> userActives = new ArrayList<>();
        for (OAuth2Authorization authorization : authorizations) {
            UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = authorization.getAttribute(Principal.class.getName());
            if (usernamePasswordAuthenticationToken != null) {
                boolean defaultUser = false;
                LocalDateTime lastLogin = null;
                DtoUserDetails userDetail = (DtoUserDetails) usernamePasswordAuthenticationToken.getPrincipal();
                if (userDetail != null) {
                    lastLogin = userDetail.getLastLogin();
                    defaultUser = userDetail.isDefaultUser();
                }

                String ipAddress = null;
                WebAuthenticationDetails details = (WebAuthenticationDetails) usernamePasswordAuthenticationToken.getDetails();
                if (details != null) {
                    ipAddress = details.getRemoteAddress();
                }

                Collection<GrantedAuthority> authorities = usernamePasswordAuthenticationToken.getAuthorities();
                List<String> roles = new ArrayList<>();
                if (authorities != null) {
                    for (GrantedAuthority authority : authorities) {
                        roles.add(authority.getAuthority());
                    }
                }

                DtoUserActive userActive = DtoUserActive.builder()
                        .id(authorization.getId())
                        .username(authorization.getPrincipalName())
                        .defaultUser(defaultUser)
                        .lastLogin(lastLogin)
                        .roles(roles.toArray(new String[0]))
                        .ipAddress(ipAddress)
                        .tokenValue(authorization.getAccessToken().getToken().getTokenValue())
                        .build();
                userActives.add(userActive);
            }
        }
        return new DtoResponse<>(StatusCode.SUCCESS, Status.SUCCESS, "Berhasil manampilkan data user yang sedang aktif", userActives);
    }

    @Override
    public DtoResponse<?> doTerminate(String id) {
        if (id == null)
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "ID harus diisi"));

        OAuth2Authorization authorization = authorizationService.findById(id);
        if (authorization == null)
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED_NOT_FOUND, Status.FAILED, "ID tidak ditemukan"));

        try {
            authorizationService.remove(authorization);
            return new DtoResponse<>(StatusCode.SUCCESS, Status.SUCCESS, "Berhasil Terminate User " + authorization.getPrincipalName());
        } catch (DataAccessException e) {
            throw serviceErrorHandling.handleDataAccessException(e, StatusCode.FAILED, Status.FAILED, "Gagal Terminate User");
        }
    }

    private String generateOTP() {
        return new DecimalFormat("0000").format(new Random().nextInt(9999));
    }

    private void validateAuthBasic() {
        host = env.getProperty("auth.host");
        if (host == null)
            host = "http://localhost:9191/oauth2/token";

        hostLogout = env.getProperty("auth.host_logout");
        if (hostLogout == null)
            hostLogout = "http://localhost:9191/oauth2/revoke";

        clientId = env.getProperty("auth.client_id");
        if (clientId == null)
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "Client ID belum di setting pada service SAC-SERVICE-SECURITY"));

        clientSecret = env.getProperty("auth.client_secret");
        if (clientSecret == null)
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "Client Secret belum di setting pada service SAC-SERVICE-SECURITY"));
    }
}
