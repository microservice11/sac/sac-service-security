package id.co.gtx.sacservicesecurity.service.impl;

import id.co.gtx.sacmodules.dto.DtoError;
import id.co.gtx.sacmodules.dto.DtoResponse;
import id.co.gtx.sacmodules.dto.ErrorCode;
import id.co.gtx.sacmodules.dto.StatusCode;
import id.co.gtx.sacmodules.enumz.Status;
import id.co.gtx.sacmodules.exception.OperationException;
import id.co.gtx.sacmodules.service.ServiceErrorHandling;
import id.co.gtx.sacmodules.utils.MapperUtil;
import id.co.gtx.sacservicesecurity.model.dto.DtoFormApp;
import id.co.gtx.sacservicesecurity.model.entity.postgresql.FormApp;
import id.co.gtx.sacservicesecurity.model.pk.FormAppPK;
import id.co.gtx.sacservicesecurity.repository.postgresql.FormAppRepository;
import id.co.gtx.sacservicesecurity.service.FormAppService;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class FormAppServiceImpl implements FormAppService {

    private final FormAppRepository formAppRepository;
    private final MapperUtil mapperUtil;
    private final ServiceErrorHandling serviceErrorHandling;

    public FormAppServiceImpl(FormAppRepository formAppRepository,
                              MapperUtil mapperUtil,
                              ServiceErrorHandling serviceErrorHandling) {
        this.formAppRepository = formAppRepository;
        this.mapperUtil = mapperUtil;
        this.serviceErrorHandling = serviceErrorHandling;
    }

    @Override
    @Transactional(readOnly = true)
    public DtoResponse<List<DtoFormApp>> findAll() {
        List<FormApp> formApps = formAppRepository.findAll();
        if (formApps.isEmpty())
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED_NOT_FOUND, Status.FAILED, "Data Form tidak ada"));

        return new DtoResponse<>(StatusCode.SUCCESS, Status.SUCCESS, "Berhasil menampilkan data Form", mapperUtil.mappingClass(formApps, DtoFormApp.class));
    }

    @Override
    @Transactional(readOnly = true)
    public DtoResponse<DtoFormApp> findByIdAndType(String id, String type) {
        return new DtoResponse<>(StatusCode.SUCCESS, Status.SUCCESS, "Berhasil menampilkan data Form By ID " + id + " dan Type "+ type, mapperUtil.mappingClass(findByPk(id, type), DtoFormApp.class));
    }

    @Override
    @Transactional(rollbackFor = {DataAccessException.class})
    public DtoResponse<DtoFormApp> createForm(DtoFormApp dtoFormApp) {
        dtoFormApp.setId("0");
        serviceErrorHandling.validateSchema(dtoFormApp).ifPresent(error -> {
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED_CREATE, Status.FAILED, "Gagal menambahkan data Form baru", null, error));
        });

        Integer idMax = formAppRepository.findByIdMax();
        String id = "0000000000" + idMax;
        dtoFormApp.setId(id.substring(id.length() - 10));

        FormApp formApp = mapperUtil.mappingClass(dtoFormApp, FormApp.class);
        formApp.setCreateAt(LocalDateTime.now());

        try {
            FormApp formAppSaved = formAppRepository.save(formApp);
            return new DtoResponse<>(StatusCode.SUCCESS_CREATE, Status.SUCCESS, "Berhasil menambakan data Form baru", mapperUtil.mappingClass(formAppSaved, DtoFormApp.class));
        } catch (DataAccessException e) {
            throw serviceErrorHandling.handleDataAccessException(e, StatusCode.FAILED_CREATE, Status.FAILED, "Gagal menambakan data Form baru");
        }
    }

    @Override
    @Transactional(rollbackFor = {DataAccessException.class})
    public DtoResponse<DtoFormApp> updateForm(DtoFormApp dtoFormApp) {
        serviceErrorHandling.validateSchema(dtoFormApp).ifPresent(error -> {
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED_UPDATE, Status.FAILED, "Gagal menambahkan data Form baru", null, error));
        });

        FormApp formApp = formAppRepository.findById(new FormAppPK(dtoFormApp.getId(), dtoFormApp.getFormType())).orElseThrow(() -> {
            DtoError error = new DtoError(ErrorCode.INVALID_PAYLOAD, "Data inputan tidak ditemukan", new ArrayList<>());
            error.addError("Field", "id", "Form ID harus diisi");
            error.addError("Field", "form_type", "Form Type harus diisi");
            return new OperationException(new DtoResponse<>(StatusCode.FAILED_UPDATE, Status.FAILED, "Gagal update data Form", null, error));
        });

        FormApp formAppUpdate = mapperUtil.mappingClass(dtoFormApp, FormApp.class);
        formAppUpdate.setCreateAt(formApp.getCreateAt());
        formAppUpdate.setUpdateAt(LocalDateTime.now());

        try {
            FormApp formAppSaved = formAppRepository.save(formAppUpdate);
            return new DtoResponse<>(StatusCode.SUCCESS_UPDATE, Status.SUCCESS, "Berhasil update data Form", mapperUtil.mappingClass(formAppSaved, DtoFormApp.class));
        } catch (DataAccessException e) {
            throw serviceErrorHandling.handleDataAccessException(e, StatusCode.FAILED_UPDATE, Status.FAILED, "Gagal update data Form");
        }
    }

    @Override
    @Transactional(rollbackFor = {DataAccessException.class})
    public DtoResponse<DtoFormApp> deleteForm(String id, String type) {
        FormApp formApp = findByPk(id, type);
        if (!formApp.getMenus().isEmpty()) {
            DtoError error = new DtoError(ErrorCode.DATA_ACCESS_EXCEPTION, "Form ID " + id + " dengan Type " + type + " sudah terbinding pada data Menu");
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED_DELETE, Status.FAILED, "Gagal hapus data Form", null, error));
        }
        try {
            formAppRepository.delete(formApp);
            return new DtoResponse<>(StatusCode.SUCCESS_DELETE, Status.SUCCESS, "Berhasil hapus data Form", mapperUtil.mappingClass(formApp, DtoFormApp.class));
        } catch (DataAccessException e) {
            throw serviceErrorHandling.handleDataAccessException(e, StatusCode.FAILED_DELETE, Status.FAILED, "Gagal hapus data Form");
        }
    }

    private FormApp findByPk(String id, String type) {
        if ("".equals(id) || id == null)
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "Path parameter Form ID harus diisi"));

        if ("".equals(type) || type == null)
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED, Status.FAILED, "Path parameter Form Type harus diisi"));

        return formAppRepository.findById(new FormAppPK(id, type)).orElseThrow(() ->
                new OperationException(new DtoResponse<>(StatusCode.FAILED_NOT_FOUND, Status.FAILED, "Form tidak ditemukan By ID " + id + " dan Type " + type))
        );
    }
}
