package id.co.gtx.sacservicesecurity.service.impl;

import id.co.gtx.sacmodules.dto.DtoGroup;
import id.co.gtx.sacmodules.utils.MapperUtil;
import id.co.gtx.sacservicesecurity.model.dto.DtoUserDetails;
import id.co.gtx.sacservicesecurity.model.entity.postgresql.Role;
import id.co.gtx.sacservicesecurity.model.entity.postgresql.User;
import id.co.gtx.sacservicesecurity.repository.postgresql.RoleRepository;
import id.co.gtx.sacservicesecurity.repository.postgresql.UserRepository;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.OAuth2Error;
import org.springframework.security.oauth2.core.OAuth2RefreshToken;
import org.springframework.security.oauth2.server.authorization.JdbcOAuth2AuthorizationService;
import org.springframework.security.oauth2.server.authorization.OAuth2Authorization;
import org.springframework.security.oauth2.server.authorization.OAuth2AuthorizationService;
import org.springframework.security.oauth2.server.authorization.client.JdbcRegisteredClientRepository;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClient;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClientRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

@Slf4j
@Primary
@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

    @Getter
    private User user;

    private final MapperUtil mapperUtil;

    private final OAuth2AuthorizationService authorizationService;

    private final RegisteredClientRepository registeredClientRepository;

    private final RoleRepository roleRepository;

    private final UserRepository userRepository;

    public UserDetailsServiceImpl(MapperUtil mapperUtil,
                                  OAuth2AuthorizationService authorizationService,
                                  RegisteredClientRepository registeredClientRepository,
                                  RoleRepository roleRepository,
                                  UserRepository userRepository) {
        this.mapperUtil = mapperUtil;
        this.authorizationService = authorizationService;
        this.registeredClientRepository = registeredClientRepository;
        this.roleRepository = roleRepository;
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        user = userRepository.findById(username)
                .orElseThrow(() ->
                        new OAuth2AuthenticationException(
                                new OAuth2Error("invalid_username", "Username is not found", null)));

        Set<GrantedAuthority> authorities = new HashSet<>();
        if (user.isDefaultUser()) {
            if (!(registeredClientRepository instanceof JdbcRegisteredClientRepository)) {
                throw new OAuth2AuthenticationException(
                        new OAuth2Error("invalid_jdbc_register_client_repository", "jdbcRegisteredClientRepository cannot null", null));
            }
            List<RegisteredClient> registeredClients = ((JdbcRegisteredClientRepository) registeredClientRepository).findAll();
            if (registeredClients.size() == 0) {
                registeredClients = Collections.singletonList(RegisteredClient.withId(UUID.randomUUID().toString()).clientId("web").build());
            }

            List<Role> roles = roleRepository.findAll();
            if (roles.size() == 0) {
                throw new OAuth2AuthenticationException(
                        new OAuth2Error("invalid_role", "Role doesn't exist", null));
            }

            registeredClients.forEach(registeredClient -> {
                roles.forEach(role -> {
                    authorities.add(new SimpleGrantedAuthority("ROLE_" + role.getName() + "_" + registeredClient.getClientId().toUpperCase()));
                });
            });
        } else {
            if (user.getResourceAccesses().size() == 0) {
                throw new OAuth2AuthenticationException(
                        new OAuth2Error("invalid_resource_access", "User doesn't have Resource Access", null));
            }

            user.getResourceAccesses().forEach(access -> {
                if (access.getRoles().size() == 0) {
                    throw new OAuth2AuthenticationException(
                            new OAuth2Error("invalid_role", "User doesn't have Role with Resource Access " + access.getClientId().toUpperCase(), null));
                }
                access.getRoles().forEach(role -> {
                    authorities.add(new SimpleGrantedAuthority("ROLE_" + role.getName() + "_" + access.getClientId().toUpperCase()));
                });
            });
        }

        if (!(authorizationService instanceof JdbcOAuth2AuthorizationService)) {
            throw new OAuth2AuthenticationException(
                    new OAuth2Error("invalid_jdbc_oauth2_authorization_service", "jdbcOAuth2AuthorizationService cannot null", null));
        }

        int maximumLogin = 0;
        List<OAuth2Authorization> auth2Authorizations = ((JdbcOAuth2AuthorizationService) authorizationService).findByPrincipalName(username);
        for (OAuth2Authorization auth2Authorization : auth2Authorizations) {
            OAuth2Authorization.Token<OAuth2RefreshToken> refreshToken = auth2Authorization.getRefreshToken();
            assert refreshToken != null;
            if (refreshToken.isActive()) {
                maximumLogin++;
            }
        }
        if (maximumLogin >= user.getLimitLogin()) {
            throw new OAuth2AuthenticationException(
                    new OAuth2Error("invalid_login", "Username " + username + " have been maximum " + user.getLimitLogin() + " login", null));
        }

        UserDetails userDetails = new DtoUserDetails(user.getUsername(), user.getPassword(),
                !user.getExpiredDate().isBefore(LocalDate.now()),
                !(user.getFailPass() >= user.getLimitFail()),
                !user.getExpiredPasswordDate().isBefore(LocalDate.now()),
                user.isEnabled(),
                authorities,
                mapperUtil.mappingClass(user.getGroups(), DtoGroup.class),
                user.isDefaultUser(),
                LocalDateTime.now());

        check(userDetails);
        return userDetails;
    }

    public void check(UserDetails user) {
        if (!user.isAccountNonLocked()) {
            log.info("Failed to authenticate since user account is locked");
            throw new OAuth2AuthenticationException(new OAuth2Error("account_locked", "User account is locked", null));
        }
        if (!user.isEnabled()) {
            log.info("Failed to authenticate since user account is disabled");
            throw new OAuth2AuthenticationException(new OAuth2Error("account_disabled", "User is disabled", null));
        }
        if (!user.isAccountNonExpired()) {
            log.info("Failed to authenticate since user account is expired");
            throw new OAuth2AuthenticationException(new OAuth2Error("account_expired", "User account has expired", null));
        }
        if (!user.isCredentialsNonExpired()) {
            log.info("Failed to authenticate since user account credentials have expired");
            throw new OAuth2AuthenticationException(new OAuth2Error("credentials_expired", "User credentials has expired", null));
        }
    }

    @Scheduled(cron = "0 0 15 * * *")
    public void removeAuthorizationNotActive() {
        log.info("remove authorization not active");
        ((JdbcOAuth2AuthorizationService) authorizationService).removeNotActive();
    }
}
