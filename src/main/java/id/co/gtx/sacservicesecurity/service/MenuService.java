package id.co.gtx.sacservicesecurity.service;

import id.co.gtx.sacmodules.dto.DtoResponse;
import id.co.gtx.sacservicesecurity.model.dto.DtoMenu;
import id.co.gtx.sacservicesecurity.model.dto.DtoMenuWeb;

import java.util.List;

public interface MenuService {
    DtoResponse<List<DtoMenu>> findAll();

    DtoResponse<DtoMenu> findByIdAndType(String id, String type);

    DtoResponse<List<DtoMenu>> findByParent();

    DtoResponse<List<DtoMenu>> findByParentId(String parentId);

    DtoResponse<DtoMenuWeb> findMenuWeb();

    DtoResponse<List<String>> getClientId();

    DtoResponse<DtoMenu> createMenu(DtoMenu dtoMenu);

    DtoResponse<DtoMenu> updateMenu(DtoMenu dtoMenu);

    DtoResponse<DtoMenu> deleteMenu(String id, String type);
}
