package id.co.gtx.sacservicesecurity.service;

import id.co.gtx.sacmodules.dto.DtoResponse;
import id.co.gtx.sacservicesecurity.model.dto.*;

import java.util.List;

public interface UserService {
    DtoResponse<List<DtoUserGet>> findAll();

    DtoResponse<DtoUserGet> findById(String username);

    DtoResponse<DtoUserGetName> findNameById(String username);

    DtoResponse<DtoUserGet> createUser(DtoUser dtoUser);

    DtoResponse<DtoUserGet> updateUser(DtoUserUpdate dtoUser);

    DtoResponse<DtoUserGet> deleteUser(String username);

    DtoResponse<DtoUserGet> passwordChange(String username, DtoPasswordChange passwordChange);

    DtoResponse<DtoUserGet> passwordReset(String username, DtoPasswordReset passwordReset);
}
