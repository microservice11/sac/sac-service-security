package id.co.gtx.sacservicesecurity.service.impl;

import id.co.gtx.sacmodules.dto.DtoResponse;
import id.co.gtx.sacmodules.dto.StatusCode;
import id.co.gtx.sacmodules.enumz.Status;
import id.co.gtx.sacmodules.exception.OperationException;
import id.co.gtx.sacmodules.service.HeadersService;
import id.co.gtx.sacmodules.utils.MapperUtil;
import id.co.gtx.sacservicesecurity.model.dto.DtoGroup;
import id.co.gtx.sacservicesecurity.model.entity.postgresql.Group;
import id.co.gtx.sacservicesecurity.repository.postgresql.GroupRepository;
import id.co.gtx.sacservicesecurity.service.GroupService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class GroupServiceImpl implements GroupService {
    private final HeadersService headers;
    private final MapperUtil mapperUtil;
    private final GroupRepository groupRepository;

    public GroupServiceImpl(HeadersService headers,
                            MapperUtil mapperUtil,
                            GroupRepository groupRepository) {
        this.headers = headers;
        this.mapperUtil = mapperUtil;
        this.groupRepository = groupRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public DtoResponse<List<DtoGroup>> findByUsername(String username) {
        List<Group> groups = groupRepository.findByUsername(username);
        if (groups.isEmpty())
            throw new OperationException(new DtoResponse<>(StatusCode.FAILED_NOT_FOUND, Status.FAILED, "Group Tidak Ditemukan By: " + username));

        return new DtoResponse<>(StatusCode.SUCCESS, Status.SUCCESS, "Group Berhasil Ditemukan By: " + username, mapperUtil.mappingClass(groups, DtoGroup.class));
    }
}
