package id.co.gtx.sacservicesecurity.controller;

import id.co.gtx.sacmodules.dto.DtoResponse;
import id.co.gtx.sacservicesecurity.model.dto.DtoFormApp;
import id.co.gtx.sacservicesecurity.service.FormAppService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/uaa/form")
public class FormAppController {

    private final FormAppService formAppService;

    public FormAppController(FormAppService formAppService) {
        this.formAppService = formAppService;
    }

    @GetMapping
    public ResponseEntity<DtoResponse<List<DtoFormApp>>> getAll() {
        return ResponseEntity.ok(formAppService.findAll());
    }

    @GetMapping("/id/{id}/type/{type}")
    public ResponseEntity<DtoResponse<DtoFormApp>> getByIdAndType(@PathVariable String id, @PathVariable String type) {
        return ResponseEntity.ok(formAppService.findByIdAndType(id, type));
    }

    @PostMapping
    public ResponseEntity<DtoResponse<DtoFormApp>> createForm(@RequestBody DtoFormApp dtoFormApp) {
        return ResponseEntity.ok(formAppService.createForm(dtoFormApp));
    }

    @PutMapping
    public ResponseEntity<DtoResponse<DtoFormApp>> updateForm(@RequestBody DtoFormApp dtoFormApp) {
        return ResponseEntity.ok(formAppService.updateForm(dtoFormApp));
    }

    @DeleteMapping("/id/{id}/type/{type}")
    public ResponseEntity<DtoResponse<DtoFormApp>> deleteForm(@PathVariable String id, @PathVariable String type) {
        return ResponseEntity.ok(formAppService.deleteForm(id, type));
    }
}
