package id.co.gtx.sacservicesecurity.controller;

import id.co.gtx.sacmodules.dto.DtoResponse;
import id.co.gtx.sacservicesecurity.model.dto.DtoGroup;
import id.co.gtx.sacservicesecurity.service.GroupService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/uaa/group")
public class GroupController {

    private final GroupService groupService;

    public GroupController(GroupService groupService) {
        this.groupService = groupService;
    }

    @GetMapping("/username/{username}")
    public ResponseEntity<DtoResponse<List<DtoGroup>>> getByUsername(@PathVariable String username) {
        return ResponseEntity.ok(groupService.findByUsername(username));
    }
}
