package id.co.gtx.sacservicesecurity.controller;

import id.co.gtx.sacmodules.dto.DtoResponse;
import id.co.gtx.sacservicesecurity.model.dto.DtoLogin;
import id.co.gtx.sacservicesecurity.model.dto.DtoOtp;
import id.co.gtx.sacservicesecurity.model.dto.DtoUserActive;
import id.co.gtx.sacservicesecurity.model.dto.DtoUserGet;
import id.co.gtx.sacservicesecurity.service.LoginService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/uaa/auth")
public class LoginController {

    private final LoginService loginService;

    public LoginController(LoginService loginService) {
        this.loginService = loginService;
    }

    @PostMapping("/check")
    public ResponseEntity<DtoResponse<DtoUserGet>> doOtp(@RequestBody DtoLogin dtoLogin) {
        return ResponseEntity.ok(loginService.doCheck(dtoLogin));
    }

    @PostMapping("/check/{otpType}")
    public ResponseEntity<DtoResponse<?>> doOtp(@RequestBody DtoLogin dtoLogin, @PathVariable String otpType) {
        return ResponseEntity.ok(loginService.doCheck(dtoLogin, otpType));
    }

    @PostMapping("/login")
    public ResponseEntity<DtoResponse<Map<String, Object>>> doLogin(@RequestBody DtoOtp dtoOtp) {
        return ResponseEntity.ok(loginService.doLogin(dtoOtp));
    }

    @PostMapping("/logout")
    public ResponseEntity<DtoResponse<?>> doLogout() {
        return ResponseEntity.ok(loginService.doLogout());
    }

    @PostMapping("/refresh")
    public ResponseEntity<DtoResponse<Map<String, Object>>> doRefresh(@RequestBody Map<String, String> mapBody) {
        return ResponseEntity.ok(loginService.doRefresh(mapBody));
    }

    @GetMapping("/active")
    public ResponseEntity<DtoResponse<List<DtoUserActive>>> userActives() {
        return ResponseEntity.ok(loginService.userActives());
    }

    @DeleteMapping("/terminate/{id}")
    public ResponseEntity<DtoResponse<?>> doTerminate(@PathVariable String id) {
        return ResponseEntity.ok(loginService.doTerminate(id));
    }
}
