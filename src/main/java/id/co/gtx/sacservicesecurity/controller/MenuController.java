package id.co.gtx.sacservicesecurity.controller;

import id.co.gtx.sacmodules.dto.DtoResponse;
import id.co.gtx.sacservicesecurity.model.dto.DtoMenu;
import id.co.gtx.sacservicesecurity.model.dto.DtoMenuWeb;
import id.co.gtx.sacservicesecurity.service.MenuService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/uaa/menu")
public class MenuController {

    private final MenuService menuService;

    public MenuController(MenuService menuService) {
        this.menuService = menuService;
    }

    @GetMapping
    public ResponseEntity<DtoResponse<List<DtoMenu>>> getAll() {
        return ResponseEntity.ok(menuService.findAll());
    }

    @GetMapping("/id/{id}/type/{type}")
    public ResponseEntity<DtoResponse<DtoMenu>> getByIdAndType(@PathVariable String id, @PathVariable String type) {
        return ResponseEntity.ok(menuService.findByIdAndType(id, type));
    }

    @GetMapping("/parent")
    public ResponseEntity<DtoResponse<List<DtoMenu>>> findByParent() {
        return ResponseEntity.ok(menuService.findByParent());
    }

    @GetMapping("/parent/{parentId}")
    public ResponseEntity<DtoResponse<List<DtoMenu>>> findByParentId(@PathVariable String parentId) {
        return ResponseEntity.ok(menuService.findByParentId(parentId));
    }

    @GetMapping("/web")
    public ResponseEntity<DtoResponse<DtoMenuWeb>> getMenuWeb() {
        return ResponseEntity.ok(menuService.findMenuWeb());
    }

    @GetMapping("/client")
    public ResponseEntity<DtoResponse<List<String>>> getClientId() {
        return ResponseEntity.ok(menuService.getClientId());
    }

    @PostMapping
    public ResponseEntity<DtoResponse<DtoMenu>> createMenu(@RequestBody DtoMenu dtoMenu) {
        return ResponseEntity.ok(menuService.createMenu(dtoMenu));
    }

    @PutMapping
    public ResponseEntity<DtoResponse<DtoMenu>> updateMenu(@RequestBody DtoMenu dtoMenu) {
        return ResponseEntity.ok(menuService.updateMenu(dtoMenu));
    }

    @DeleteMapping("/id/{id}/type/{type}")
    public ResponseEntity<DtoResponse<DtoMenu>> deleteMenu(@PathVariable String id, @PathVariable String type) {
        return ResponseEntity.ok(menuService.deleteMenu(id, type));
    }
}
