package id.co.gtx.sacservicesecurity.controller;

import id.co.gtx.sacmodules.dto.DtoResponse;
import id.co.gtx.sacservicesecurity.model.dto.DtoRole;
import id.co.gtx.sacservicesecurity.service.RoleService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/uaa/role")
public class RoleController {

    private final RoleService roleService;

    public RoleController(RoleService roleService) {
        this.roleService = roleService;
    }

    @GetMapping
    public ResponseEntity<DtoResponse<List<DtoRole>>> getAll() {
        return ResponseEntity.ok(roleService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<DtoResponse<DtoRole>> getById(@PathVariable String id) {
        return ResponseEntity.ok(roleService.findById(id));
    }

    @PostMapping
    public ResponseEntity<DtoResponse<DtoRole>> createRole(@RequestBody DtoRole dtoRole) {
        return ResponseEntity.ok(roleService.createRole(dtoRole));
    }

    @PutMapping
    public ResponseEntity<DtoResponse<DtoRole>> updateRole(@RequestBody DtoRole dtoRole) {
        return ResponseEntity.ok(roleService.updateRole(dtoRole));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<DtoResponse<DtoRole>> deleteRole(@PathVariable String id) {
        return ResponseEntity.ok(roleService.deleteRole(id));
    }
}
