package id.co.gtx.sacservicesecurity.controller;

import id.co.gtx.sacmodules.dto.DtoResponse;
import id.co.gtx.sacservicesecurity.model.dto.*;
import id.co.gtx.sacservicesecurity.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/uaa/user")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public ResponseEntity<DtoResponse<List<DtoUserGet>>> getAll() {
        return ResponseEntity.ok(userService.findAll());
    }

    @GetMapping("/{username}")
    public ResponseEntity<DtoResponse<DtoUserGet>> getById(@PathVariable String username) {
        return ResponseEntity.ok(userService.findById(username));
    }

    @PostMapping
    public ResponseEntity<DtoResponse<DtoUserGet>> createUser(@RequestBody DtoUser dtoUser) {
        return ResponseEntity.status(HttpStatus.CREATED).body(userService.createUser(dtoUser));
    }

    @PutMapping
    public ResponseEntity<DtoResponse<DtoUserGet>> updateUser(@RequestBody DtoUserUpdate dtoUser) {
        return ResponseEntity.ok(userService.updateUser(dtoUser));
    }

    @DeleteMapping("/{username}")
    public ResponseEntity<DtoResponse<DtoUserGet>> deleteUser(@PathVariable String username) {
        return ResponseEntity.ok(userService.deleteUser(username));
    }

    @PostMapping("/password/change/{username}")
    public ResponseEntity<DtoResponse<DtoUserGet>> passwordChange(@PathVariable String username, @RequestBody DtoPasswordChange passwordChange) {
        return ResponseEntity.ok(userService.passwordChange(username, passwordChange));
    }

    @PostMapping("/password/reset/{username}")
    public ResponseEntity<DtoResponse<DtoUserGet>> passwordReset(@PathVariable String username, @RequestBody DtoPasswordReset passwordReset) {
        return ResponseEntity.ok(userService.passwordReset(username, passwordReset));
    }
}
